/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : wdtpl

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-11-21 14:13:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `companies_owner_id_users_id` (`owner_id`),
  CONSTRAINT `companies_owner_id_users_id` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', 'JuristConsult', 'Decebal 6', '076700062', '1', '1');
INSERT INTO `companies` VALUES ('2', 'TestCompany', 'Stefan 9', '076701081', '7', '1');
INSERT INTO `companies` VALUES ('3', 'Biroul notarului Turuta Marin', 'mun. Chișinău, str. Sf. Andrei 20', '023556132', '9', '1');

-- ----------------------------
-- Table structure for companies_members
-- ----------------------------
DROP TABLE IF EXISTS `companies_members`;
CREATE TABLE `companies_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rights` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_memeber_id_companies_id` (`company_id`),
  KEY `companies_memeber_id_users_id` (`user_id`),
  CONSTRAINT `companies_memeber_id_companies_id` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `companies_memeber_id_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_members
-- ----------------------------
INSERT INTO `companies_members` VALUES ('2', '1', '9', '1');

-- ----------------------------
-- Table structure for companies_requests
-- ----------------------------
DROP TABLE IF EXISTS `companies_requests`;
CREATE TABLE `companies_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT current_timestamp(),
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `description` varchar(1000) DEFAULT '',
  `answered` tinyint(1) DEFAULT NULL,
  `answer` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `afdafbasbgnsfnbasdfas` (`company_id`),
  CONSTRAINT `afdafbasbgnsfnbasdfas` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_requests
-- ----------------------------
INSERT INTO `companies_requests` VALUES ('1', '3', '2019-11-10 23:06:19', 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062', 'Am o problemă foarte gravă', '1', null);
INSERT INTO `companies_requests` VALUES ('2', '1', '2019-11-14 21:39:24', 'Vasilică', 'Șprot', 'gingiraca@incom.lac', '903', 'Mi-a destupat cineva conserva!', '0', null);

-- ----------------------------
-- Table structure for companies_reserves
-- ----------------------------
DROP TABLE IF EXISTS `companies_reserves`;
CREATE TABLE `companies_reserves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `reserve_date` date DEFAULT NULL,
  `datetime_start` datetime DEFAULT NULL,
  `duration` tinyint(3) DEFAULT NULL,
  `reserve_meet_index` int(11) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ajkbvasbvhslkaodihdsjgshl` (`company_id`),
  CONSTRAINT `ajkbvasbvhslkaodihdsjgshl` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_reserves
-- ----------------------------
INSERT INTO `companies_reserves` VALUES ('1', '1', '2019-11-02 21:31:09', '2019-11-05', null, null, '2', null, null, null, null);
INSERT INTO `companies_reserves` VALUES ('2', '1', null, '2019-11-17', null, null, '3', null, null, null, null);
INSERT INTO `companies_reserves` VALUES ('3', '1', '2019-11-10 21:42:38', '2019-11-14', '2019-11-14 08:00:00', '20', null, 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062');
INSERT INTO `companies_reserves` VALUES ('4', '3', '2019-11-10 21:48:18', '2019-11-12', '2019-11-12 08:00:00', '30', null, 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062');
INSERT INTO `companies_reserves` VALUES ('5', '3', '2019-11-10 21:50:34', '2019-11-16', '2019-11-12 11:00:00', '30', null, 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062');
INSERT INTO `companies_reserves` VALUES ('6', '2', '2019-11-10 21:52:51', '2019-11-14', '2019-11-14 11:00:00', '40', null, 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062');
INSERT INTO `companies_reserves` VALUES ('7', '3', '2019-11-10 22:09:34', '2019-11-16', '2019-11-12 09:30:00', '30', null, 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062');
INSERT INTO `companies_reserves` VALUES ('8', '1', '2019-11-11 08:44:11', '2019-11-12', '2019-11-12 11:00:00', '35', null, 'Sandu', 'Morari', 'sandu.m.md@gmail.com', '076700062');
INSERT INTO `companies_reserves` VALUES ('9', '1', '2019-11-14 20:13:32', '2019-11-14', '2019-11-14 23:00:00', '40', null, 'Aliona', 'Dast', 'alioncik@mail.ru', '076701081');

-- ----------------------------
-- Table structure for companies_schedule
-- ----------------------------
DROP TABLE IF EXISTS `companies_schedule`;
CREATE TABLE `companies_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `week_day` tinyint(1) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `stop_time` time DEFAULT NULL,
  `p_start_time` time DEFAULT NULL,
  `p_stop_time` time DEFAULT NULL,
  `meet_duration` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_schedule_c_id_companies_id` (`company_id`),
  CONSTRAINT `companies_schedule_c_id_companies_id` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_schedule
-- ----------------------------
INSERT INTO `companies_schedule` VALUES ('1', '1', '1', '08:00:00', '18:00:00', '13:00:00', '14:00:00', '30');
INSERT INTO `companies_schedule` VALUES ('2', '1', '2', '08:00:00', '18:00:00', '13:00:00', '14:00:00', '30');
INSERT INTO `companies_schedule` VALUES ('3', '1', '4', '09:00:00', '17:00:00', '13:00:00', '13:40:00', '30');
INSERT INTO `companies_schedule` VALUES ('4', '1', '7', '14:00:00', '17:00:00', '15:30:00', '15:50:00', '20');
INSERT INTO `companies_schedule` VALUES ('5', '3', '2', '08:00:00', '13:00:00', '14:00:00', '18:00:00', '0');
INSERT INTO `companies_schedule` VALUES ('6', '2', '3', '08:00:00', '13:00:00', '14:00:00', '18:00:00', null);
INSERT INTO `companies_schedule` VALUES ('7', '2', '4', '08:00:00', '13:00:00', '14:00:00', '18:00:00', null);

-- ----------------------------
-- Table structure for companies_services
-- ----------------------------
DROP TABLE IF EXISTS `companies_services`;
CREATE TABLE `companies_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `service_type` enum('consult','registration') DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `duration` tinyint(3) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `companies_services_id_companies_id` (`company_id`),
  CONSTRAINT `companies_services_id_companies_id` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_services
-- ----------------------------
INSERT INTO `companies_services` VALUES ('1', '1', 'consult', 'Consultație juridică', 'Consultație juridică(ceva descriere)', '15', '1');
INSERT INTO `companies_services` VALUES ('2', '1', 'consult', 'Consultație notarială', 'Epta', '20', '1');
INSERT INTO `companies_services` VALUES ('3', '2', 'consult', 'TestService', 'TestServiceDescription', '40', '1');
INSERT INTO `companies_services` VALUES ('4', '1', 'consult', 'Ți-ai pregătit acikou pentru Pizdiucan?', 'Nu nah', '25', '1');
INSERT INTO `companies_services` VALUES ('5', '3', 'consult', 'Perfectarea unor acte notariale', 'Procură, contracte, cereri ș.a.', '30', '1');
INSERT INTO `companies_services` VALUES ('6', '3', 'consult', 'Procura', 'Perfecrare procurilor', '30', '1');
INSERT INTO `companies_services` VALUES ('7', '3', 'consult', 'Cereri', 'Perfectarea actelor de tip cerere', '30', '1');
INSERT INTO `companies_services` VALUES ('8', '3', 'consult', 'Avizuri', 'Crearea avizurilor', '20', '1');
INSERT INTO `companies_services` VALUES ('9', '3', 'consult', 'Declaratii', 'Declaratii pe venit si altele', '50', '1');
INSERT INTO `companies_services` VALUES ('10', '3', 'consult', 'Invitatii', 'Formularea invitatiilor', '15', '1');
INSERT INTO `companies_services` VALUES ('11', '3', 'consult', 'Perfectarea unor acte notariale', 'Procură, contracte, cereri ș.a.', '30', '1');
INSERT INTO `companies_services` VALUES ('12', '3', 'consult', 'Procura', 'Perfecrare procurilor', '30', '1');
INSERT INTO `companies_services` VALUES ('13', '3', 'consult', 'Cereri', 'Perfectarea actelor de tip cerere', '30', '1');
INSERT INTO `companies_services` VALUES ('14', '3', 'consult', 'Avizuri', 'Crearea avizurilor', '20', '1');
INSERT INTO `companies_services` VALUES ('15', '3', 'consult', 'Declaratii', 'Declaratii pe venit si altele', '50', '1');
INSERT INTO `companies_services` VALUES ('16', '3', 'consult', 'Invitatii', 'Formularea invitatiilor', '15', '1');
INSERT INTO `companies_services` VALUES ('17', '3', 'consult', 'Perfectarea unor acte notariale', 'Procură, contracte, cereri ș.a.', '30', '1');
INSERT INTO `companies_services` VALUES ('18', '3', 'consult', 'Procura', 'Perfecrare procurilor', '30', '1');
INSERT INTO `companies_services` VALUES ('19', '3', 'consult', 'Cereri', 'Perfectarea actelor de tip cerere', '30', '1');
INSERT INTO `companies_services` VALUES ('20', '3', 'consult', 'Avizuri', 'Crearea avizurilor', '20', '1');
INSERT INTO `companies_services` VALUES ('21', '3', 'consult', 'Declaratii', 'Declaratii pe venit si altele', '50', '1');
INSERT INTO `companies_services` VALUES ('22', '3', 'consult', 'Invitatii', 'Formularea invitatiilor', '15', '1');

-- ----------------------------
-- Table structure for companies_services_docs
-- ----------------------------
DROP TABLE IF EXISTS `companies_services_docs`;
CREATE TABLE `companies_services_docs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fasbagfasdvvfawe` (`service_id`),
  CONSTRAINT `fasbagfasdvvfawe` FOREIGN KEY (`service_id`) REFERENCES `companies_services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_services_docs
-- ----------------------------
INSERT INTO `companies_services_docs` VALUES ('1', '1', 'Cerere', 'Cerere.docx', '1');
INSERT INTO `companies_services_docs` VALUES ('2', '2', 'Procura', 'Procura.docx', '1');
INSERT INTO `companies_services_docs` VALUES ('3', '4', 'Document 3', 'file.txt', '0');
INSERT INTO `companies_services_docs` VALUES ('4', '5', 'Procura pe automobil', 'Procura.docx', '1');
INSERT INTO `companies_services_docs` VALUES ('6', '1', 'Procura', 'Procura pe automobil.docx', '1');
INSERT INTO `companies_services_docs` VALUES ('7', '1', 'Proc', 'Cerere2.docx', '1');
INSERT INTO `companies_services_docs` VALUES ('8', '1', 'Cere', 'Cerere3.docx', '1');

-- ----------------------------
-- Table structure for companies_services_requests
-- ----------------------------
DROP TABLE IF EXISTS `companies_services_requests`;
CREATE TABLE `companies_services_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_service_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT current_timestamp(),
  `client_email` varchar(100) DEFAULT NULL,
  `client_subject` varchar(100) DEFAULT NULL,
  `client_description` varchar(1000) DEFAULT NULL,
  `answered` tinyint(1) DEFAULT 0,
  `answer` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_services_requests_id_companies_services_id` (`company_service_id`),
  CONSTRAINT `companies_services_requests_id_companies_services_id` FOREIGN KEY (`company_service_id`) REFERENCES `companies_services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of companies_services_requests
-- ----------------------------
INSERT INTO `companies_services_requests` VALUES ('9', '1', '2019-10-24 17:43:33', 'sandu.m.md@gmail.com', 'Epta', 'Eblnaceg', '1', 'Belesti pula, iaca ce');
INSERT INTO `companies_services_requests` VALUES ('10', '1', '2019-10-25 00:24:43', 'sandu.m.md@gmail.com', 'Epta', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', '0', null);
INSERT INTO `companies_services_requests` VALUES ('11', '2', '2019-10-25 10:11:53', 'saniok.i.am@gmail.com', 'Desigur ca vrea', 'Vreau o pulâ cât o șurâ', '0', null);
INSERT INTO `companies_services_requests` VALUES ('12', '1', '2019-10-25 14:21:47', 'sandu.m.md@gmail.com', 'Inca un epta', 'Da nu nahui...', '1', 'Da nu nah');
INSERT INTO `companies_services_requests` VALUES ('13', '3', '2019-10-25 16:40:07', 'sandu.m.md@gmail.com', 'Epta', 'Eblan', '1', 'Tu tot nah te duci');
INSERT INTO `companies_services_requests` VALUES ('14', '2', '2019-10-25 16:51:24', 'sandu.m.md@gmail.com', 'Da', 'Da de 4 puteti?', '1', 'Nu nah');
INSERT INTO `companies_services_requests` VALUES ('15', '2', '2019-10-25 21:11:05', 'sandu.m.md@gmail.com', 'Inca un epta', 'svera face probleme?', '1', 'Epta');
INSERT INTO `companies_services_requests` VALUES ('16', '4', '2019-10-26 09:28:03', 'sandu.m.md@gmail.com', 'Inca nu', 'Tre sa cumpar vazilina', '0', null);
INSERT INTO `companies_services_requests` VALUES ('17', '1', '2019-10-30 17:50:43', 'marin.turuta@gmail.com', 'procura pe masina', 'am un mercedes culoare alba cu 2 cai legati de pula', '0', null);
INSERT INTO `companies_services_requests` VALUES ('18', '1', '2019-10-30 18:32:49', 'asdasdasda', 'sadasdas', 'asdasduhuasdhasdiasdadasd', '0', null);
INSERT INTO `companies_services_requests` VALUES ('19', '5', '2019-10-30 22:01:01', 'sandu.m.md@gmail.com', 'Inca un epta', 'Da nu nah', '0', null);

-- ----------------------------
-- Table structure for jurist_consult_requests
-- ----------------------------
DROP TABLE IF EXISTS `jurist_consult_requests`;
CREATE TABLE `jurist_consult_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `client_email` varchar(100) DEFAULT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `response` varchar(1000) DEFAULT NULL,
  `responded` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `jurist_consult_id_users_to_user_id` (`to_user_id`),
  CONSTRAINT `jurist_consult_id_users_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jurist_consult_requests
-- ----------------------------
INSERT INTO `jurist_consult_requests` VALUES ('7', '2019-10-24 01:52:45', '1', 'sandu.m.md@gmail.com', 'Ce pla mea sa fac?', 'Pula', '0', 'Tu tot nah te duci', '1');
INSERT INTO `jurist_consult_requests` VALUES ('8', '2019-10-24 01:54:14', '7', 'sandu.m.md@gmail.com', 'Pot sa aflu mai multe detalii?', 'Da', '0', 'Desigur, cel mai mic e de 35cm', '1');
INSERT INTO `jurist_consult_requests` VALUES ('9', '2019-10-24 09:57:36', '7', 'sandu.m.md@gmail.com', 'Da nu aveti mai mare?', 'Canesna', '0', 'Canesna ca avem de la 40cm pana la 110cm', '1');
INSERT INTO `jurist_consult_requests` VALUES ('10', '2019-10-24 10:00:36', '7', 'a@a.a', 'Epta', 'Da nu nahui', '1', null, '0');
INSERT INTO `jurist_consult_requests` VALUES ('11', '2019-10-24 10:02:13', '7', 'sandu.m.md@gmail.com', 'Vreau o pizda mai mare, ca sa incapa trei puli odata', 'Epta', '1', null, '0');
INSERT INTO `jurist_consult_requests` VALUES ('12', '2019-10-24 10:38:55', '7', 'sandu.m.md@gmail.com', 'Datimio!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', 'Vreau!Vreau!Vreau!', '1', null, '0');

-- ----------------------------
-- Table structure for jurist_consult_services
-- ----------------------------
DROP TABLE IF EXISTS `jurist_consult_services`;
CREATE TABLE `jurist_consult_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jurist_consult_services_user_id_users_id` (`user_id`),
  CONSTRAINT `jurist_consult_services_user_id_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jurist_consult_services
-- ----------------------------
INSERT INTO `jurist_consult_services` VALUES ('1', '1', '1', 'Ai nevoie de o consultatie juridica?');
INSERT INTO `jurist_consult_services` VALUES ('2', '7', '1', 'Vrei un dildo pe mărimea ta?');
INSERT INTO `jurist_consult_services` VALUES ('3', '7', '1', 'Mareste pula de 3 ori');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_datetime` datetime DEFAULT NULL,
  `stop_datetime` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL,
  `active` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `userId` (`user_id`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('158', '2019-10-25 14:10:33', '2019-10-25 14:35:33', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('159', '2019-10-25 14:18:22', '2019-10-25 14:43:22', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('160', '2019-10-25 14:43:41', '2019-10-25 15:08:41', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('161', '2019-10-25 15:20:09', '2019-10-25 15:45:09', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('162', '2019-10-25 15:20:20', '2019-10-25 15:45:20', '7', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('163', '2019-10-25 15:55:41', '2019-10-25 16:20:41', '7', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('164', '2019-10-25 16:24:13', '2019-10-25 16:49:13', '7', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('165', '2019-10-25 16:36:48', '2019-10-25 17:01:48', '7', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('166', '2019-10-25 16:51:35', '2019-10-25 17:16:35', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('167', '2019-10-25 17:17:56', '2019-10-25 17:42:56', '7', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('168', '2019-10-25 17:18:07', '2019-10-25 17:43:07', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('169', '2019-10-25 18:11:46', '2019-10-25 18:36:46', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('170', '2019-10-25 21:11:17', '2019-10-25 21:36:17', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('171', '2019-10-26 09:20:20', '2019-10-26 09:45:20', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('172', '2019-10-26 12:50:14', '2019-10-26 13:15:14', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('173', '2019-10-30 17:47:53', '2019-10-30 18:12:53', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('174', '2019-10-30 18:30:52', '2019-10-30 18:55:52', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('175', '2019-10-30 19:17:59', '2019-10-30 19:42:59', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('176', '2019-10-30 22:01:09', '2019-10-30 22:26:09', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('177', '2019-10-31 00:03:18', '2019-10-31 00:28:18', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('178', '2019-10-31 00:22:37', '2019-10-31 00:47:37', '10', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('179', '2019-10-31 00:31:27', '2019-10-31 00:56:27', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('180', '2019-11-01 11:53:54', '2019-11-01 12:18:54', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('181', '2019-11-01 12:24:45', '2019-11-01 12:49:45', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('182', '2019-11-01 17:49:26', '2019-11-01 18:14:26', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('183', '2019-11-01 18:31:12', '2019-11-01 18:56:12', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('184', '2019-11-01 18:47:04', '2019-11-01 19:12:04', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('185', '2019-11-01 19:13:00', '2019-11-01 19:38:00', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('186', '2019-11-01 19:26:19', '2019-11-01 19:51:19', '7', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('187', '2019-11-01 19:27:02', '2019-11-01 19:52:02', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('188', '2019-11-02 13:03:43', '2019-11-02 13:28:43', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('189', '2019-11-02 15:28:12', '2019-11-02 15:53:12', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('190', '2019-11-02 16:13:46', '2019-11-02 16:38:46', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('191', '2019-11-05 23:24:06', '2019-11-05 23:49:06', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('192', '2019-11-06 00:21:08', '2019-11-06 00:46:08', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('193', '2019-11-09 21:59:33', '2019-11-09 22:24:33', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('194', '2019-11-09 22:05:27', '2019-11-09 22:30:27', '11', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('195', '2019-11-09 22:05:48', '2019-11-09 22:30:48', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('196', '2019-11-10 00:35:34', '2019-11-10 01:00:34', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('197', '2019-11-10 22:08:20', '2019-11-10 22:33:20', '1', '178.168.16.9', '1');
INSERT INTO `sessions` VALUES ('198', '2019-11-10 22:08:24', '2019-11-10 22:33:24', '1', '178.168.16.9', '1');
INSERT INTO `sessions` VALUES ('199', '2019-11-14 22:14:17', '2019-11-14 22:39:17', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('200', '2019-11-14 22:14:45', '2019-11-14 22:39:45', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('201', '2019-11-16 18:25:04', '2019-11-16 18:50:04', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('202', '2019-11-16 18:25:10', '2019-11-16 18:50:10', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('203', '2019-11-17 01:05:23', '2019-11-17 01:30:23', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('204', '2019-11-17 01:08:03', '2019-11-17 01:33:03', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('205', '2019-11-17 01:10:02', '2019-11-17 01:35:02', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('206', '2019-11-17 01:10:17', '2019-11-17 01:35:17', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('207', '2019-11-17 01:12:03', '2019-11-17 01:37:03', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('208', '2019-11-17 01:26:46', '2019-11-17 01:51:46', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('209', '2019-11-17 04:50:33', '2019-11-17 05:15:33', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('210', '2019-11-17 04:51:07', '2019-11-17 05:16:07', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('211', '2019-11-18 12:43:43', '2019-11-18 13:08:43', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('212', '2019-11-18 12:49:38', '2019-11-18 13:14:38', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('213', '2019-11-18 13:25:48', '2019-11-18 13:50:48', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('214', '2019-11-18 13:29:15', '2019-11-18 13:54:15', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('215', '2019-11-18 13:38:21', '2019-11-18 14:03:21', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('216', '2019-11-18 13:40:57', '2019-11-18 14:05:57', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('217', '2019-11-19 16:29:22', '2019-11-19 16:54:22', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('218', '2019-11-19 16:54:54', '2019-11-19 17:19:54', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('219', '2019-11-19 16:59:51', '2019-11-19 17:24:51', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('220', '2019-11-19 17:24:30', '2019-11-19 17:49:30', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('221', '2019-11-19 17:51:41', '2019-11-19 18:16:41', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('222', '2019-11-19 18:27:11', '2019-11-19 18:52:11', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('223', '2019-11-19 18:47:18', '2019-11-19 19:12:18', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('224', '2019-11-19 19:13:58', '2019-11-19 19:38:58', '9', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('225', '2019-11-19 19:14:55', '2019-11-19 19:39:55', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('226', '2019-11-19 19:41:16', '2019-11-19 20:06:16', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('227', '2019-11-19 19:36:23', '2019-11-19 20:01:23', '1', '178.168.16.9', '1');
INSERT INTO `sessions` VALUES ('228', '2019-11-19 19:37:14', '2019-11-19 20:02:14', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('229', '2019-11-20 20:41:53', '2019-11-20 21:06:53', '1', '127.0.0.1', '1');
INSERT INTO `sessions` VALUES ('230', '2019-11-20 23:56:15', '2019-11-21 00:21:15', '1', '127.0.0.1', '1');

-- ----------------------------
-- Table structure for templates
-- ----------------------------
DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of templates
-- ----------------------------
INSERT INTO `templates` VALUES ('1', '2019-09-25 21:55:24', 'Template', '_2019_09_25_21_55_24_626855.docx', '1');
INSERT INTO `templates` VALUES ('2', '2019-09-25 21:56:46', 'Template', '_2019_09_25_21_56_46_408782.docx', '0');
INSERT INTO `templates` VALUES ('3', '2019-09-25 21:57:32', 'Template', '_2019_09_25_21_57_32_915696.docx', '1');
INSERT INTO `templates` VALUES ('4', '2019-09-25 22:00:17', 'Template', '1_2019_09_25_22_00_17_202575.docx', '1');
INSERT INTO `templates` VALUES ('5', '2019-09-26 09:50:54', 'Cerere', '1_2019_09_26_09_50_54_323470.docx', '0');
INSERT INTO `templates` VALUES ('6', '2019-09-26 09:53:13', 'Cerere 2', '1_2019_09_26_09_53_13_607914.docx', '1');
INSERT INTO `templates` VALUES ('7', '2019-09-26 09:57:59', 'marin', '1_2019_09_26_09_57_59_165385.docx', '0');
INSERT INTO `templates` VALUES ('8', '2019-09-26 10:03:38', 'marin 2', '1_2019_09_26_10_03_38_657442.docx', '1');
INSERT INTO `templates` VALUES ('9', '2019-09-26 10:05:32', 'mturuta', '1_2019_09_26_10_05_32_176698.docx', '1');
INSERT INTO `templates` VALUES ('10', '2019-09-26 10:26:17', 'Test', '3_2019_09_26_10_26_17_613767.docx', '1');
INSERT INTO `templates` VALUES ('11', '2019-09-27 00:09:58', 'Test', '1_2019_09_27_00_09_58_821575.docx', '1');
INSERT INTO `templates` VALUES ('12', '2019-09-27 10:52:56', 'Template', '1_2019_09_27_10_52_56_463919.docx', '1');
INSERT INTO `templates` VALUES ('13', '2019-09-27 11:02:39', 'Epta', '1_2019_09_27_11_02_39_268183.docx', '1');
INSERT INTO `templates` VALUES ('14', '2019-09-27 12:45:06', 'Template', '1_2019_09_27_12_45_06_503387.docx', '1');
INSERT INTO `templates` VALUES ('15', '2019-10-01 08:15:42', 'Da nu nahui', '1_2019_10_01_08_15_42_358749.docx', '1');
INSERT INTO `templates` VALUES ('16', '2019-10-01 23:33:04', 'Eblan', '1_2019_10_01_23_33_04_647492.docx', '1');
INSERT INTO `templates` VALUES ('17', '2019-10-01 23:58:40', 'Procura', '1_2019_10_01_23_58_40_353132.docx', '1');
INSERT INTO `templates` VALUES ('18', '2019-10-02 00:26:12', 'Test', '1_2019_10_02_00_26_12_302877.docx', '1');
INSERT INTO `templates` VALUES ('19', '2019-10-02 00:51:45', 'Procura pe automobil', '1_2019_10_02_00_51_45_852324.docx', '1');
INSERT INTO `templates` VALUES ('20', '2019-10-02 08:18:13', 'Eblan', '1_2019_10_02_08_18_13_725996.docx', '1');
INSERT INTO `templates` VALUES ('21', '2019-10-05 21:50:26', 'Test', '1_2019_10_05_21_50_26_961619.docx', '1');
INSERT INTO `templates` VALUES ('22', '2019-10-05 23:35:23', 'Test', '7_2019_10_05_23_35_23_589678.docx', '1');
INSERT INTO `templates` VALUES ('23', '2019-10-05 23:43:08', 'Test', '7_2019_10_05_23_43_08_478402.docx', '1');
INSERT INTO `templates` VALUES ('24', '2019-10-06 23:43:50', 'Nume', '8_2019_10_06_23_43_50_226978.docx', '1');
INSERT INTO `templates` VALUES ('25', '2019-10-06 23:43:52', 'Nume', '8_2019_10_06_23_43_52_117058.docx', '1');
INSERT INTO `templates` VALUES ('26', '2019-10-24 20:34:32', 'Procura', '7_2019_10_24_20_34_32_879555.docx', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(3) NOT NULL,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `billed` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `users_user_types_ky` (`type_id`),
  CONSTRAINT `users_user_types_ky` FOREIGN KEY (`type_id`) REFERENCES `user_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'sandu', 'Sandu', 'Morari', 'b0202b85e00fcb4e73640bf524dffc99', '1', '1');
INSERT INTO `users` VALUES ('2', '1', 's', '', '', '03c7c0ace395d80182db07ae2c30f034', '0', '0');
INSERT INTO `users` VALUES ('3', '1', 'Tester', '', '', '202cb962ac59075b964b07152d234b70', '0', '0');
INSERT INTO `users` VALUES ('6', '1', 'pidar', '', '', '6f6cb5951fe3cbf02251cbe5c3369f52', '0', '0');
INSERT INTO `users` VALUES ('7', '1', 'test', '', '', '098f6bcd4621d373cade4e832627b4f6', '0', '0');
INSERT INTO `users` VALUES ('8', '1', 'Radius', '', '', '1b3231655cebb7a1f783eddf27d254ca', '0', '0');
INSERT INTO `users` VALUES ('9', '1', 'marin', 'Marin', 'Turuta', 'bc2630c8bb9f2fad2ab86d7b2c7c0361', '0', '0');
INSERT INTO `users` VALUES ('10', '1', 'epta', '', '', '4aed7227767e7e176c3645e1ad13f62b', '0', '0');
INSERT INTO `users` VALUES ('11', '1', 'eptaeptaeptaepta', '', '', '4aed7227767e7e176c3645e1ad13f62b', '0', '0');

-- ----------------------------
-- Table structure for users_templates
-- ----------------------------
DROP TABLE IF EXISTS `users_templates`;
CREATE TABLE `users_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `template_id` (`template_id`),
  CONSTRAINT `users_templates_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `users_templates_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_templates
-- ----------------------------
INSERT INTO `users_templates` VALUES ('1', '1', '4', '0');
INSERT INTO `users_templates` VALUES ('2', '1', '5', '0');
INSERT INTO `users_templates` VALUES ('3', '1', '6', '0');
INSERT INTO `users_templates` VALUES ('4', '1', '7', '0');
INSERT INTO `users_templates` VALUES ('5', '1', '8', '0');
INSERT INTO `users_templates` VALUES ('6', '1', '9', '0');
INSERT INTO `users_templates` VALUES ('7', '3', '10', '0');
INSERT INTO `users_templates` VALUES ('8', '1', '11', '0');
INSERT INTO `users_templates` VALUES ('9', '1', '12', '0');
INSERT INTO `users_templates` VALUES ('10', '1', '13', '0');
INSERT INTO `users_templates` VALUES ('11', '1', '14', '0');
INSERT INTO `users_templates` VALUES ('12', '1', '15', '0');
INSERT INTO `users_templates` VALUES ('13', '1', '16', '0');
INSERT INTO `users_templates` VALUES ('14', '1', '17', '0');
INSERT INTO `users_templates` VALUES ('15', '1', '18', '0');
INSERT INTO `users_templates` VALUES ('16', '1', '19', '1');
INSERT INTO `users_templates` VALUES ('17', '1', '20', '0');
INSERT INTO `users_templates` VALUES ('18', '1', '21', '0');
INSERT INTO `users_templates` VALUES ('19', '7', '22', '0');
INSERT INTO `users_templates` VALUES ('20', '7', '23', '0');
INSERT INTO `users_templates` VALUES ('21', '8', '24', '0');
INSERT INTO `users_templates` VALUES ('22', '8', '25', '0');
INSERT INTO `users_templates` VALUES ('23', '7', '26', '1');

-- ----------------------------
-- Table structure for user_types
-- ----------------------------
DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_types
-- ----------------------------
INSERT INTO `user_types` VALUES ('1', 'admin');
