function searchTemplates(container, pageToShow, eachInPage, actionsPermited, searchWord = false) {
    $.post("/templates/get", {page: pageToShow, each: eachInPage, search: searchWord}, function(data, status){
        if (status == "success") {
            try {
                var resp = JSON.parse(data);
                container.empty();
                if (resp.ok) {
                    $.each(resp.body, function(key, value) {
                        var html = '<div class="list-item circle-left circle-right">';
                        
                        if (actionsPermited.run) {
                            html += '<div class="btn-ico btn-ico-md middle circle spaced-right" id="tpl_run_' + key + '">&#9658;</div>';
                        }

                        if (actionsPermited.remove) {
                            html += '<div class="btn-ico btn-ico-md middle circle btn-rm spaced-right" id="tpl_rmv_' + key + '">&#10006;</div>';
                        }

                        if (actionsPermited.edit) {
                            html += '<div class="btn-ico btn-ico-md btn-edit circle spaced-right" id="tpl_edit_' + key + '">&#9998;</div>';
                        }

                        if (actionsPermited.title) {
                            html += '<div class="text text-md middle">' + value["name"] + '</div>';
                        }

                        html += '</div>';
                        container.append(html);

                        if (actionsPermited.run) {
                            $('#tpl_run_' + key).click(function(){
                                runTemplate(key);
                            });
                        }

                        if (actionsPermited.remove) {
                            $('#tpl_rmv_' + key).click(function(){
                                removeTemplate(key, value["name"]);
                            });
                        }

                        if (actionsPermited.edit) {
                            $('#tpl_edit_' + key).click(function(){
                                editTemplate(key);
                            });
                        }
                    });
                } else {
                    console.log(resp.info.message);
                }
            } catch (ex) {
                console.log(ex.message);
            }
        } else {
            console.log(status);
        }  
    });
}

function removeTemplate(templateId, templateName)
{
    modal({
        title: "Ștergeți '" + templateName + "'?",
        closable: true,
        fields: {
            templateId: {
                type: "hidden",
                value: templateId
            }
        },
        controls: {
            Da: {
                type: "modal-window-confirm",
                action: {
                    type: "request",
                    method: "post",
                    url: "/templates/remove",
                    ok: {
                        type: "redirect",
                        url: "/manager"
                    },
                    notok: {
                        type: "alert",
                        message: "removeTemplate bad response"
                    }
                }
            },
            Nu: {
                type: "modal-window-close"
            }
        }
    });
}

function editTemplate(templateId)
{
    modal({ 
        title: "Editarea nu este disponibilă",
        closable: true,
        controls: {
            Inchide: {
                type: "modal-window-close"
            }
        }
    });
}

function runTemplate(id)
{
    $.post("/templates/getVariables", {templateId: id}, function(data){
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                modal({
                    elementId: "modal",
                    title: resp.info.templateName,
                    closable: true,
                    fields: resp.body,
                    controls: {
                        Proceseaza: {
                            type: "modal-window-confirm",
                            action: {
                                type: "download-file",
                                url: "/templates/process",
                            }
                        }
                    }
                });
            } else {
                console.log("Bad resp");
            }
        } catch (ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}