function getJuristConsultServices()
{
    var container = $("[list-juristConsultServices]");
    var html = "";

    $.post("/home/getJuristConsultServices", {}, function(data, status){
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                $.each(resp.body, function(key, value){
                    var btnId = makeId(10)+'btn'+key;
                    var btnCloseId = makeId(10)+'btnClose'+key;
                    var formId = makeId(10)+'form'+key;
                    var bannerId = makeId(10)+'banner'+key;
                    var bodyId = makeId(10)+'body'+key;

                    html = '<div class="banner spaced-after" id="'+bannerId+'">'
                    html += '<span>'+value.title+'(de la '+value.username+')'+'</span>'
                    html += '<div class="banner-body spaced-before" id="'+bodyId+'">'
                    html += '<form id="'+formId+'" enctype="multipart/form-data" onsubmit="return false;">'
                    html += '<input type="text" name="title" placeholder="Tema problemei">'
                    html += '<input type="text" name="email" placeholder="Your email address">'
                    html += '<input type="hidden" name="username" value="'+value.username+'">'
                    html += '<textarea name="problem" cols="25" rows="8" placeholder="Your problem"></textarea>'
                    html += '<button class="btn" id="'+btnId+'">Trimite</button>'
                    html += '</form>'
                    html += '</div>'
                    html += '</div>'

                    container.append(html);

                    $("#"+btnCloseId).click(function(e){
                        $("#"+bodyId).hide();
                    });

                    $("#"+bannerId).click(function(e){
                        $(".banner-body").hide();
                        $("#"+bodyId).show();
                    });

                    $("#"+btnId).click(function(e){
                        modal({
                            title: "Sigur doriti sa trimiteti?",
                            closable: true,
                            controls: {
                                Da: {
                                    type: "modal-window-confirm",
                                    action: {
                                        type: "request", 
                                        url: "/home/juristConsult",
                                        formId: formId, 
                                        ok: {
                                            type: "redirect-success",
                                            url: "/home"
                                        },
                                        notok: {
                                            type: "alert",
                                            message: "Eroare la trimitere"
                                        }
                                    }
                                },
                                Nu: {
                                    type: "modal-window-close"
                                }
                            }
                        });
                    });
                });
            } else {
                console.log(resp.info.message);
            }
        } catch (ex) {
            console.log(ex.message);
        }
    });
}


function getCompaniesServices()
{
    var container = $("[list-juristConsultServices]");
    var html = "";

    $.post("/services/consult", {}, function(data){
        // console.log(data);
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                $.each(resp.body, function(key, value){
                    var btnId = makeId(10)+'btn'+key;
                    var btnCloseId = makeId(10)+'btnClose'+key;
                    var formId = makeId(10)+'form'+key;
                    var bannerId = makeId(10)+'banner'+key;
                    var bodyId = makeId(10)+'body'+key;

                    html = '<div class="banner spaced-after" id="'+bannerId+'">'
                    html += '<span>'+value.service_title+'(de la '+value.company_name+')'+'</span>'
                    html += '<div class="banner-body spaced-before" id="'+bodyId+'">'
                    html += '<form id="'+formId+'" enctype="multipart/form-data" onsubmit="return false;">'
                    html += '<input type="text" name="title" placeholder="Tema problemei">'
                    html += '<input type="text" name="email" placeholder="Your email address">'
                    html += '<input type="hidden" name="service" value="'+key+'">'
                    html += '<textarea name="problem" cols="25" rows="8" placeholder="Your problem"></textarea>'
                    html += '<button class="btn" id="'+btnId+'">Trimite</button>'
                    html += '</form>'
                    html += '</div>'
                    html += '</div>'

                    container.append(html);

                    $("#"+btnCloseId).click(function(e){
                        $("#"+bodyId).hide();
                    });

                    $("#"+bannerId).click(function(e){
                        $(".banner-body").hide();
                        $("#"+bodyId).show();
                    });

                    $("#"+btnId).click(function(e){
                        modal({
                            title: "Sigur doriti sa trimiteti?",
                            closable: true,
                            controls: {
                                Da: {
                                    type: "modal-window-confirm",
                                    action: {
                                        type: "request", 
                                        url: "/services/sendRequest",
                                        formId: formId, 
                                        ok: {
                                            type: "redirect-success",
                                            url: "/home"
                                        },
                                        notok: {
                                            type: "alert",
                                            message: "Eroare la trimitere"
                                        }
                                    }
                                },
                                Nu: {
                                    type: "modal-window-close"
                                }
                            }
                        });
                    });
                });
            } else {
                console.log(resp.info.message);
            }
        } catch (ex) {
            console.log(ex.message);
        }
    });
}