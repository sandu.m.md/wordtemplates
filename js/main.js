function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function makeId(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function modal(obj)
{
    var modalId = "modal";
    
    if (!$("#"+modalId).length) {
        $("body").append('<div id="'+modalId+'" class="modal-cover hidden"><div>');
    }

    var md = $("#"+modalId);
    md.empty();
    var html = "";
    var actions = {};

    obj.closable = obj.hasOwnProperty("closable") ? obj.closable : false;

    // set title
    var titleHtml = '<div class="modal-title">' + obj.title + '</div>';

    // set description
    var descriptionHtml = obj.hasOwnProperty("description") ? '<div class="modal-description">'+obj.description+'</div>' : "";

    // set fields
    var fieldsHtml = "";
    
    if (obj.hasOwnProperty("fields")) {
        for(const [key, value] of Object.entries(obj.fields)) {
            if (value.hasOwnProperty("textarea")) {
                fieldsHtml += '<textarea rows="'+(descriptionHtml.length/100)+'" style="width:100%;" name="'+key+'"';

                for(const[attrName, attrValue] of Object.entries(value)) {
                    fieldsHtml += ' ' + attrName + '="' + attrValue + '"';
                }

                fieldsHtml += '></textarea>';
            } else {
                fieldsHtml += '<input name="' + key + '"';

                for(const[attrName, attrValue] of Object.entries(value)) {
                    fieldsHtml += ' ' + attrName + '="' + attrValue + '"';
                }

                fieldsHtml += '>';
            }
        }
    }

    // set controls
    var controlsHtml = "";
    if (obj.hasOwnProperty("controls")) {
        for(const [key, value] of Object.entries(obj.controls)) {
            controlsHtml += '<button class="btn btn-lg" id="' + value.type + '">' + key + '</button>';
            
            if (value.hasOwnProperty('action')) {
                actions[value.type] = value.action;
            }
        }
    }

    // set closable
    var closeHtml = obj.closable ? '<div id="modal-window-close-x" class="modal-close">&#10006;</div>' : '';

    // set modal window type
    var modalWindowClass = "modal-window";
    modalWindowClass += obj.hasOwnProperty("type") ? " "+obj.type : "";

    html = '<div id="modal-window" class="'+modalWindowClass+'">' +
                titleHtml + 
                descriptionHtml +
                (fieldsHtml != '' ? '<form id="modal-window-form" enctype="multipart/form-data">' + fieldsHtml + '</form>' : '') +
                controlsHtml +
                closeHtml +
            '</div>';


    md.empty();
    md.append(html);

    for(const [key, value] of Object.entries(actions)) {
        $('#' + key).click(function(){
            if (value.hasOwnProperty("formId")) {
                doAction(value, value.formId);    
            } else {
                doAction(value, "modal-window-form");
            }
        });
    }

    $('#modal-window-cancel').click(function(){
        md.hide();
    });

    $('#modal-window-close').click(function(){
        md.hide();
    });

    $('#modal-window-close-x').click(function(){
        md.hide();
    });

    $("#modal-window").keypress(function(e){
        if (e.keycode == 13 || e.which == 13) {
            $("#modal-window-confirm").trigger("click");
        }
    });

    md.click(function(e){
    	if (document.getElementById("modal-window").contains(e.target)){
    		// Clicked in box
    	} else {
    		// Clicked outside the box
    		md.hide();
    	}
    });

    md.show();
}

function doAction(action, formId)
{
    // notif("Entered");
    // return false;

    switch(action.type) {
        case 'reload':
            window.location.replace(window.location.href);
        case 'upload-request':
                var file_data = $('#upload-file').prop('files')[0];   
                var form_data = new FormData($("#"+formId)[0]);                  
                form_data.append('file', file_data);
    
                $.ajax({
                    url: action.url,
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(data){
                        try {
                            var resp = JSON.parse(data);
    
                            if (resp.ok) {
                                doAction(action.ok)
                            } else {
                                doAction(action.notok);
                            }
                        } catch (ex) {
                            console.log(ex.message);
                        }
                    }
                });
    
    
            break;
        case 'request':
            $.post(action.url, $("#"+formId).serializeArray(), function(data, status){
                try {
                    var resp = JSON.parse(data);
                    if (resp.ok) {
                        
                        notif("Acțiune cu success");
                        setTimeout(doAction(action.ok), 10000);
                    } else {
                        // doAction(action.notok);
                        notif(resp.info.message);
                    }
                } catch (ex) {
                    console.log(ex.message);
                    console.log(data);
                }
            });
            break;

        case "redirect":
            window.location.replace(action.url);
            break;
        
        case "redirect-success": 
            alert("Operatiune cu succes")
            window.location.replace(action.url);
            break;

        case "download-file":
            $.AjaxDownloader({
                url  : action.url,
                data : $("#"+formId).serializeArray()
            });
            break;

        case "refresh-manager":
                searchTemplates($('#templates-list'), 1, 10, {run: false, remove: true, edit: true, title: true}, document.getElementById('search-box').value);
            break;

        case "alert":
            console.log(action.message);
            break;

        default:
            break;
    }
}

function iframe(obj)
{
    var modalId = "modal-iframe";
    
    if (!$("#"+modalId).length) {
        $("body").append('<div id="'+modalId+'" class="modal-cover hidden"><div>');
    }

    var md = $("#"+modalId);
    md.empty();
    var html = "";
    var actions = {};

    // obj.closable = obj.hasOwnProperty("closable") ? obj.closable : false;
    var closeHtml =  '<div id="modal-window-close-x" class="modal-close">&#10006;</div>';

    // set modal window type
    var modalWindowClass = "iframe-window";

    html = '<div class="'+modalWindowClass+'" id="iframe-window">' +
                closeHtml +
            '</div>';

    md.empty();
    md.append(html);

    $.post(obj.url, {}, function(data){
        $("#iframe-window").html(data);
    });

    $('#modal-window-close-x').click(function(){
        md.hide();
    });

    md.click(function(e){
    	if (document.getElementById("iframe-window").contains(e.target)){
    		// Clicked in box
    	} else {
    		// Clicked outside the box
    		md.hide();
    	}
    });

    md.show();
}

function loadInPage(obj) {
    // console.log(obj.url);
    // console.log(obj.selector);
    $.post(obj.url, {}, function(data){
        // console.log(data);
        $(obj.selector).html(data);
    });
}

function cal(date = "", companyId, prev = false, next = false) {
    if (date == "") {
        date = new Date().getFullYear() + "/" + (new Date().getMonth() + 1);
    }

    $(".calendar-box-month-prev").hide();
    $(".calendar-box-month-next").hide();

    if (prev) $(".calendar-box-month-prev").show();
    if (next) $(".calendar-box-month-next").show();

    $.post("/util/cal/"+companyId+"/"+date, {}, function(data) {
        var cal = $(".calendar-box-days");
        cal.empty();

        try {
            var resp = JSON.parse(data);
            if (resp.ok) {
                $(".calendar-box-month-name").html(resp.body.month + " " + resp.body.year);

                for(var weekOfMonth = 0; weekOfMonth < resp.body.cal.length; weekOfMonth++) {
                    cal.append('<div class="calendar-box-days-row"></div>');
                    var week = $(".calendar-box-days-row:last-child");
        
                    for(var dayOfWeek = 1; dayOfWeek < 8; dayOfWeek++) {
                        if (resp.body.cal[weekOfMonth] != null && resp.body.cal[weekOfMonth][dayOfWeek] != null) {                            
                            week.append('<span onclick="getSchedule($(\'#day-sch\'), this, '+companyId+');" day="'+resp.body.cal[weekOfMonth][dayOfWeek][0].split("_")[1]+'" ' + resp.body.cal[weekOfMonth][dayOfWeek].join(" ") + ' >' + parseInt(resp.body.cal[weekOfMonth][dayOfWeek][0].split("-")[2]) + '</span>');
                        } else {
                            week.append('<span hidden_day>0</span>');
                        }
                    }
                }

                $($("[selected-day]")[0]).trigger("click");
            } else {
                console.log(resp.info.message);
            }
        } catch (ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}

function getSchedule(container, el, companyId) {
    var day = $(el).attr("day");
    $("[inactive]").removeAttr("selected-day");
    $(el).attr("selected-day", '');
    // $("input[name=reserve_date]").val(new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + day);
    // console.log(day);
    $.post("/companies/getDaySchedule/"+companyId, {date: day}, function(data) {
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                if (resp.body !== true && resp.body !== false) {
                    container.empty();
                    container.parent().show();
                    $.each(resp.body, function(key, value) {
                        var id = makeId(15);

                        container.append('<div id="'+id+'" class="day-sch-el">'+value+'</div>');

                        $("#"+id).click(function(e){
                            $("#meeting-info").html("Veți veni pe "+day.replace(/\-/g,'.')+", la ora "+value);
                            $("input[name=datetime_start]").val(new Date().getFullYear() + "/" + (new Date().getMonth() + 1) + "/" + day + " " + value);
                            $("#finish-info").html("You will come at " + value + " for " + $("input[name=duration]").val() + ' minutes');
                            $("#register-info").html("You will come at " + value + " for " + $("input[name=duration]").val() + ' minutes');
                            $("#register-form").show();
                            $("#finish-btn").show();
                            $("#btn-next-3").show();
                        });
                    });  
                } else {
                    console.log("Empty body");
                }
            } else {
                console.log(resp.info.message);
            }

        } catch (ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}

function loadIntoTable(url, table, actions) {
    $.post(url, {}, function(data) {
        try {
            var resp = JSON.parse(data);
            
            if (resp.ok) {
                if (resp.body != true && resp.body !== false) {
                    table.empty();

                    table.append('<tr><th>' + resp.body.fields.join('</th><th>') + '</th>'+(actions.length?'<th colspan="'+actions.length+'">Actions</th>':'')+'</tr>');

                    $.each(resp.body.tableData, function(rowId, row) {
                        var html = '';

                        $.each(row, function(tableDataKey, tableData) {
                            html += '<td>'+tableData+'</td>';
                        });

                        $.each(actions, function(key, value) {
                            if (value == "r") {
                                html += '<td><div class="bx-full bx-flex bx-content-center pointer fa fa-info-circle" onclick="loadInto(\'/companies/getDocView/'+rowId+'\', $(\'#pageadd\'));"></div></td>'
                            } else if (value == "d") {
                                html += '<td><div class="bx-full bx-flex bx-content-center pointer fa fa-remove" onclick="doAction({type:\'request\', url: \'/templates/remove/'+rowId+'\', ok: {type:\'reload\'}});"></div></td>';
                            } else if (value == "e") {
                                // html += '<td><div class="bx-full bx-flex bx-content-center pointer fa fa-edit" onclick="$(this).parent().attr(\'contenteditable\', \'true\');"></div></td>';
                            }
                        })

                        table.append('<tr>' + html + '</tr>');
                    });
                } else {
                    table.empty();
                    table.append('<tr><th>Nu sunt înregistrări</th></tr>');
                }
            } else {
                console.log(resp.info.message);
            }
        } catch(ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}

function loadInto(url, container) {
    $.post(url, {}, function(data) { 
        container.html(data);   
    })
}

function btnClick(btn) {
    var par = btn.parent();
    par.children(".btn").removeClass("btn-selected");
    btn.addClass("btn-selected");
}

function bxBtnClick(btn) {
    var par = btn.parent();
    par.children(".bx-btn").removeClass("bx-btn-selected");
    btn.addClass("bx-btn-selected");
}

function topBtnClick(btn) {
    var par = btn.parent();
    par.children(".top-btn").removeClass("top-btn-selected");
    btn.addClass("top-btn-selected");
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
  
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
  
function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}

function notif(text, type = "alert", sec = 5) {
    if (!$("#notif").length) {
        $("body").append('<div id="notif"></div>');
    }

    var notif = $("#notif");
    hideNotif();

    notif.hide();
    notif.empty();
    notif.addClass("bx-flex-row abs abs-l abs-b bx-mg bx-bg-dark-1 bx-txt-white bx-pad");
    notif.append('<div class="bx-flex-el bx-flex bx-content-center bx-single-line fp-0">'+text+'</div>');
    notif.append('<div class="bx-flex-el bx-pad-l bx-flex bx-content-center fp-0 pointer" onclick="hideNotif()">&#10006;</div>');
    
    setTimeout(function(){
        hideNotif();
    }, sec*1000);
    
    hideNotif();
    notif.show();
    notif.css("left", "0.5em");
}

function hideNotif() {
    var notif = $("#notif");
    notif.css("left", "-500px");
}
