function authenticate() {
    modal({
        elementId: "modal",
        title: "Autentificare",
        closable: true,
        fields: {
            username: {
                type: "text",
                placeholder: "Username"
            },
            password: {
                type: "password",
                placeholder: "Password"
            }
        },
        controls: {
            Confirm: {
                type: 'modal-window-confirm',
                action: {
                    type: "request",
                    method: "post",
                    url: "/api/login",
                    ok: {
                        type: "redirect",
                        url: "/manager"
                    },
                    notok: {
                        type: "alert",
                        message: "Ati introdus ceva incorect"
                    }
                }
            },
            Cancel: {
                type: 'modal-window-cancel'
            }
        }
    });
}