function getConsultRequests() {
    var container = $("[list-juristConsultRequests]");
    var html = "";

    $.post("/services/consultRequests", {}, function(data){
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                container.empty();

                if (resp.body === true || resp.body === false) {
                    container.append("Nu aveti inscrieri")
                } else {
                    $.each(resp.body, function(key, value){
                        var btnId = makeId(10) + key;
                        
                        html = "";
                        html += '<div class="list-item circle-left circle-right">';
                        html += '<div class="btn-ico btn-ico-md middle circle btn-add" id="'+btnId+'">&#10531;</div>';
                        html += '<div class="text text-md middle">'+value.client_subject+'</div>';
                        html += '</div>';

                        container.append(html);

                        $("#"+btnId).click(function(e){
                            modal({
                                title: value.client_subject,
                                description: value.client_description,
                                closable: true,
                                fields: {
                                    response: {
                                        // type: "text",
                                        textarea: true,
                                        placeholder: "Scrie un răspuns",
                                        autocomplete: "off"
                                    },
                                    request: {
                                        type: "hidden",
                                        value: key
                                    }
                                },
                                controls: {
                                    Trimite: {
                                        type: "modal-window-confirm",
                                        action: {
                                            type: "request",
                                            url: "/services/sendServiceResponse",
                                            ok: {
                                                type: "reload"
                                            },
                                            notok: {
                                                type: "alert",
                                                message: "unsuccessful"
                                            }
                                        }
                                    }
                                }
                            });
                        });
                    });
                }
            } else {
                console.log(resp.info.message);
            }
        } catch(ex) {
            console.log("Exception: " + ex.message);
            console.log(data);
        }
    });
}

function getCompaniesMenu() {
    var element = $("[menu-companies]");

    $.post("/services/getCompanies", {}, function(data){
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                if (resp.body !== false && resp.body !== true) {
                    element.empty();
                    element.append('<div class="menu-title">Companies</div>');
                    
                    $.each(resp.body, function(key, value){
                        var companyId = makeId(10)+"cmp"+key;
                        var html = '';//'<a href="/companies/show/1">';

                        html += '<div class="menu-item" id="'+companyId+'">';
                        html += '<div class="menu-item-ico menu-item-ico-edit">&#10070;</div>';
                        html += '<div class="menu-item-title">'+value.company_name+'</div>'
                        html += '</div>';

                        element.append(html);

                        $("#"+companyId).click(function(){
                            iframe({
                                closable: true,
                                url: "/companies/show/"+key
                            });
                        });

                        // $("#"+companyId).click(function(e){
                        //     modal({
                        //         title: "Add service",
                        //         closable: true,
                        //         type: "modal-window-lg",
                        //         fields: {
                        //             service_type: {
                        //                 type: "hidden",
                        //                 value: "consult"
                        //             },
                        //             company_id: {
                        //                 type: "hidden",
                        //                 value: key
                        //             },
                        //             service_name: {
                        //                 type: "text",
                        //                 placeholder: "Service name",
                        //                 autocomplete: "off"
                        //             },
                        //             service_description: {
                        //                 type: "text",
                        //                 placeholder: "Write some description",
                        //                 autocomplete: "off"
                        //             }
                        //         },
                        //         controls: {
                        //             Add: {
                        //                 type: "modal-window-confirm",
                        //                 action: {
                        //                     type: "request",
                        //                     url: "/services/addService",
                        //                     ok: {
                        //                         type: "reload"
                        //                     },
                        //                     notok: {
                        //                         type: "alert",
                        //                         message: "Unsuccessful"
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     });
                        // });
    
    
    
                    });

                } else {
                    var btnId = makeId(10)+"btn";
                    element.empty();
                    element.append('<div class="menu-item" id="'+btnId+'"><div class="menu-item-ico menu-item-ico-add">&#10010;</div><div class="menu-item-title">Add company</div></div>');
                    $("#"+btnId).click(function(e){
                        modal({
                            title: "Add company",
                            closable: true,
                            fields: {
                                company_name: {
                                    type: "text",
                                    placeholder: "Company name",
                                    autocomplete: "off"
                                },
                                company_address: {
                                    type: "text",
                                    placeholder: "Company address",
                                    autocomplete: "off"
                                }
                            },
                            controls: {
                                Add: {
                                    type: "modal-window-confirm",
                                    action: {
                                        type: "request",
                                        url: "/services/addCompany",
                                        ok: {
                                            type: "reload",
                                        },
                                        notok: {
                                            type: "alert",
                                            message: "Unsuccessful"
                                        }
                                    }
                                }
                            }
                        });
                    });
                } 
            } else {
                console.log(resp.info.message);
            }
        } catch (ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}

function getServicesMenu() {
    var element = $("[menu-services]");

    $.post("/services/getServices", {}, function(data){
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                if (resp.body !== true && resp.body !== false) {
                    element.empty();
                    element.append('<div class="menu-title">Services</div>');
                    $.each(resp.body, function(key, value){
                        var serviceId = makeId(10)+"btn"+key;
                        var html = "";

                        html += '<div class="menu-item" id="'+serviceId+'">'
                        html += '<div class="menu-item-ico menu-item-ico-edit">&#10070;</div>';
                        html += '<div class="menu-item-title">'+value.service_name+'</div>';

                        element.append(html);

                        $("#"+serviceId).click(function(e){
                            modal({
                                title: value.service_name,
                                description: value.service_description,
                                controls: {
                                    Close: {
                                        type: "modal-window-close"
                                    }
                                }
                            });
                        });
                    });
                } else {
                    element.hide();
                }
            } else {
                console.log(res.info.message);
            }
        } catch (ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}