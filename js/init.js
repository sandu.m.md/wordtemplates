function start() {
    // $("[navbar-logo").click(function(){
    //     window.location.replace("/home");
    // });

    $("[link-home]").click(function(){
        window.location.replace("/home")
    });

    $("[link-templates]").click(function(){
        window.location.replace("/templates");
    });

    $("[link-manager]").click(function(){
        window.location.replace("/manager");
    });

    $("[link-about]").click(function(){
        window.location.replace("/about");
    });

    // $("#navbar-user-item-login").click(function(){
    //     window.location.replace("/auth/login");
    // });

    // $("#navbar-user-item-signup").click(function(){
    //     window.location.replace("/auth/signup")
    // });

    $("[link-register]").click(function(){
        modal({
            title: "Inregistrare",
            closable: true,
            fields: {
                username: {
                    type: "text",
                    placeholder: "Username",
                    autocomplete: "off"
                },
                password: {
                    type: "password",
                    placeholder: "Password",
                    autocomplete: "off"
                }
            },
            controls: {
                Register: {
                    type: "modal-window-confirm",
                    action: {
                        type: "request",
                        method: "post",
                        url: "/auth/register",
                        ok: {
                            type: "redirect",
                            url: "/home"
                        },
                        notok: {
                            type: "alert",
                            message: "Inregistrarea a esuat"
                        }
                    }
                }
            }
        });
    });

    $("[link-login]").click(function(){
        modal({
            title: "Autentificare",
            closable: true,
            fields: {
                username: {
                    type: "text",
                    placeholder: "Username"
                },
                password: {
                    type: "password",
                    placeholder: "Password"
                }
            },
            controls: {
                Confirm: {
                    type: 'modal-window-confirm',
                    action: {
                        type: "request",
                        method: "post",
                        url: "/auth/login",
                        ok: {
                            type: "redirect",
                            url: "/manager"
                        },
                        notok: {
                            type: "alert",
                            message: "Autentificarea a eșuat"
                        }
                    }
                }
            }
        });
    });

    $("[link-logout]").click(function(){
        doAction({
            type: "request",
            url: "/auth/logout",
            ok: {
                type: "redirect",
                url: "/home"
            },
            notok: {
                type: "alert",
                message: "Logout error"
            }
        });
    });


    $("[link-my-cab]").click(function(e){
        window.location.replace("/me");
    });

    $("[link-main]").click(function(e){
        window.location.replace("/");
    });
}
