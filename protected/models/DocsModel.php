<?php 

class DocsModel
{
    public static function getById($docId)
    {
        $sql = "SELECT * FROM companies_services_docs WHERE id = $docId AND active = 1";
        
        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            return array($docId => $result[$docId]);
        }

        return false;
    }

    public static function _getAllByCompanyId($companyId, $serviceId = 0)
    {
        $sql = "SELECT `csd`.`id`, `csd`.`name` `Denumire fișier`, `csd`.`filename` Fisier FROM companies c INNER JOIN companies_services cs ON `c`.`id` = `cs`.`company_id` INNER JOIN companies_services_docs csd ON `cs`.`id` = `csd`.`service_id` WHERE `c`.`id` = $companyId AND `csd`.`active` = 1" . ($serviceId ? " AND `cs`.`id` = $serviceId" : "");

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();
            $i = 1;
            foreach ($result as $row) {
                $id = $row["id"];
                unset($row["id"]); 
                $arr[$id] = array_merge(array("N/o"=>$i), $row);
                $i++;
            }

            return $arr;
        }

        return $result;
    }

    public static function deactivateById($docId)
    {
        $sql = "UPDATE `companies_services_docs` SET `active` = 0 WHERE id = $docId";
        
        if (!(App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return true;
    }
}



?>