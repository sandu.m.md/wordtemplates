<?php 

class ServicesModel
{
    public static function getAllActive($where = false)
    {
        $sql = "SELECT * FROM ".self::$tableName." WHERE `active` = 1".($where?" AND $where ":"");

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result)) {
            $arr = array();

            foreach($result as $id => $row) {
                $arr[$id] = $row;
            }

            return $arr;
        }

        return true;
    }

    public static function getFullAllActive($where = false)
    {
        $sql = "SELECT `cs`.`id`, `c`.`name` `company_name`, `c`.`address` `company_address`, `u`.`username` `company_owner`, `cs`.`service_type`, `cs`.`title` `service_title`,`cs`.`description` `service_description` FROM `companies_services` `cs` INNER JOIN `companies` `c` ON `cs`.`company_id` = `c`.`id` INNER JOIN `users` `u` ON `c`.`owner_id` = `u`.`id` WHERE `cs`.`active` = 1 AND `c`.`active` = 1 ".($where?" AND $where ":"");
        
        if (!($result = App::$db->query($sql))) {

            return false;
        }

        if (is_array($result)) {
            $arr = array();

            foreach($result as $id => $row) {
                $arr[$row["id"]] = $row;
            }

            return $arr;
        }

        return true;
    }

    public static function getById($id)
    {
        return self::getAllActive("id=$id");
    }

    public static function getFullById($id)
    {
        return self::getFullAllActive("cs.id=$id");
    }

    public static function isAllowedToUser($userId, $serviceId)
    {
        $sql = "SELECT
                    count(*) cnt
                FROM
                    (
                        SELECT
                            cs.id
                        FROM
                            companies_services cs
                        INNER JOIN companies c ON cs.company_id = c.id
                        INNER JOIN companies_members cm ON c.id = cm.company_id
                        INNER JOIN users u ON cm.user_id = u.id
                        OR c.owner_id = u.id
                        WHERE
                            c.active = 1
                        AND cs.active = 1
                        AND u.id = $userId
                        AND cs.id = $serviceId
                        UNION
                            SELECT
                                cs.id
                            FROM
                                companies_services cs
                            INNER JOIN companies c ON cs.company_id = c.id
                            INNER JOIN users u ON c.owner_id = u.id
                            OR c.owner_id = u.id
                            WHERE
                                c.active = 1
                            AND cs.active = 1
                            AND u.id = $userId
                            AND cs.id = $serviceId
                    ) AS a
        ";

        App::$log->error($sql);

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return $result[""]["cnt"] ? true : false;
    }

    public static function add($companyId, $serviceType, $serviceName, $serviceDescription)
    {
        $sql = "INSERT INTO companies_services(company_id, service_type, title, description) VALUES ($companyId, '$serviceType', '$serviceName', '$serviceDescription')";

        if (!App::$db->query($sql)) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return true;
    }

    public static function getAllByUserId($userId)
    {
        $sql = "SELECT
                    cs.id,
                    cs.title service_name,
                    cs.description service_description
                FROM
                    companies c
                INNER JOIN companies_members cm ON c.id = cm.company_id
                INNER JOIN companies_services cs ON cs.company_id = c.id
                INNER JOIN users u ON cm.user_id = u.id
                WHERE
                    u.id = $userId
                UNION
                    SELECT
                        cs.id,
                        cs.title service_name,
                        cs.description service_description
                    FROM
                        companies c
                    INNER JOIN companies_services cs ON cs.company_id = c.id
                    INNER JOIN users u ON c.owner_id = u.id
                    WHERE
                        u.id = $userId
        ";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result)) {
            $arr = array();

            foreach($result as $row) {
                $arr[$row["id"]] = $row;
            }

            return $arr;
        }

        return true;
    }

    public static function getDocsByServiceId($serviceId)
    {
        $sql = "SELECT * FROM companies_services_docs WHERE service_id = $serviceId";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return true;
        }

        // var_dump($sql);

        if (!is_array($result)) {
            return array();
        }

        $arr = array();
        foreach ($result as $doc) {
            $arr[$doc["id"]] = $doc;
        }

        return $result;
    }

    public static function getAllByCompanyId($companyId)
    {
        $sql = "SELECT
                    *
                FROM
                    companies_services
                WHERE
                    company_id = $companyId
                AND active = 1";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (!is_array($result)) {
            return array();
        }

        $arr = array();
        foreach ($result as $serv) {
            $arr[$serv["id"]] = array_merge($serv, array("docs" => self::getDocsByServiceId(($serv["id"]))));
        }

        return $arr;
    }

    public static function _getAllByCompanyId($companyId)
    {
        $sql = "SELECT id, title `Categorie`, description `Descriere` FROM companies_services WHERE company_id = $companyId AND active = 1";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();
            $i = 1;
            foreach ($result as $serv) {
                $id = $serv["id"];
                unset($serv["id"]);
                $arr[$id] = array_merge(array("N/o"=>$i), $serv);//array_merge($serv, array("docs" => self::getDocsByServiceId(($serv["id"]))));
                $i++;
            }
            return $arr;
        }

        return false;;
    }
}

?>