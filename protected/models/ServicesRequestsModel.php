<?php 

class ServicesRequestsModel
{
    public static function add($service_id, $client_email, $client_subject, $client_description)
    {
        $sql = "INSERT INTO `companies_services_requests`(`company_service_id`, `client_email`, `client_subject`, `client_description`) VALUES ($service_id, '$client_email', '$client_subject', '$client_description')";
    
        if (!(App::$db->query($sql))) {
            App::$log->error("SQL Error: $sql");
            return false;
        }

        return true;
    }

    public static function getAllByUserId($userId)
    {
        $sql = "SELECT
                    csr.id,
                    cs.title service_subject,
                    csr.client_email,
                    csr.client_subject,
                    csr.client_description
                FROM
                    companies_services_requests csr
                INNER JOIN companies_services cs ON csr.company_service_id = cs.id
                INNER JOIN companies c ON cs.company_id = c.id
                INNER JOIN companies_members cm ON c.id = cm.company_id
                INNER JOIN users u ON cm.user_id = u.id
                OR c.owner_id = u.id
                WHERE
                    c.active = 1
                AND csr.answered = 0
                AND cs.active = 1
                AND u.id = $userId
                UNION
                    SELECT
                        csr.id,
                        cs.title service_subject,
                        csr.client_email,
                        csr.client_subject,
                        csr.client_description
                    FROM
                        companies_services_requests csr
                    INNER JOIN companies_services cs ON csr.company_service_id = cs.id
                    INNER JOIN companies c ON cs.company_id = c.id
                    INNER JOIN users u ON c.owner_id = u.id
                    OR c.owner_id = u.id
                    WHERE
                        c.active = 1
                    AND csr.answered = 0
                    AND cs.active = 1
                    AND u.id = $userId
        ";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result)) {
            $arr = array();
            foreach ($result as $row) {
                $arr[$row["id"]] = $row;
            }

            return $arr;
        }

        return true;

    }

    public static function getById($id)
    {
        $sql = "select * from companies_services_requests where id = $id limit 1";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            return end($result);
        }

        return false;
    }

    public static function answer($id, $answer)
    {
        $sql = "update companies_services_requests set answered = 1, answer = '$answer' where id = $id";

        if (!App::$db->query($sql)) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return true;
    }
}


?>