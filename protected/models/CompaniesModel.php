<?php

class CompaniesModel
{
    public static function _getAllMembersByCompanyId($companyId)
    {
        $sql = "SELECT `u`.`username` FROM companies_members cm INNER JOIN users u ON `cm`.`user_id` = `u`.`id` WHERE company_id = $companyId";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();
            $i = 1;
            foreach ($result as $row) {
                $arr[] = array_merge(array("N/o"=>$i), $row);
                $i++;
            }

            return $arr;
        }

        return $result;
    }

    public static function getAllByUserId($userId)
    {
        $sql = "select c.id, c.name, c.address, c.phone_number from companies c inner join companies_members cm on c.id = cm.company_id inner join users u on cm.user_id = u.id where u.id = $userId union select c.id, c.name, c.address, c.phone_number from companies c inner join users u on c.owner_id = u.id where u.id = $userId";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result)) {
            $arr = array();

            foreach($result as $row) {
                $arr[$row["id"]] = $row;
            }

            return $arr;
        }

        return true;
    }

    public static function add($userId, $companyName, $companyAddress)
    {
        $sql = "INSERT INTO companies(owner_id, name, address) VALUES ($userId, '$companyName', '$companyAddress')";

        if (!App::$db->query($sql)) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return true;
    }

    public static function isAllowedtoUser($userId, $companyId)
    {
        $sql = "SELECT
                    count(*) cnt
                FROM
                    (
                        SELECT
                            c.id
                        FROM
                            companies c
                        INNER JOIN companies_members cm ON c.id = cm.company_id
                        INNER JOIN users u ON cm.user_id = u.id
                        WHERE
                            u.id = $userId
                        AND c.id = $companyId
                        UNION
                            SELECT
                                c.id
                            FROM
                                companies c
                            INNER JOIN users u ON c.owner_id = u.id
                            WHERE
                                u.id = $userId
                            AND c.id = $companyId
                    ) AS a
        ";

        if (!($result = App::$db->query($sql))) {
            App::$log->error(__CLASS__, __FUNCTION__, "SQL Error $sql");
            return false;
        }

        return $result[""]["cnt"] ? true : false;
    }

    public static function getById($companyId)
    {
        $sql = "SELECT
                    c.id,
                    `name`,
                    `address`,
                    u.username owner_name,
                    c.phone_number
                FROM
                    companies c
                INNER JOIN users u ON u.id = c.owner_id
                WHERE
                    c.id = $companyId";
        
        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return count($result) ? $result[$companyId] : false;
    }

    public static function getReservesByCompanyId($companyId, $date)
    {
        $sql = "SELECT
                    cr.reserve_date date,
                    cr.reserve_meet_index id
                FROM
                    companies_reserves cr
                INNER JOIN companies c ON cr.company_id = c.id
                WHERE
                    c.id = $companyId
                    AND cr.reserve_date = '$date'";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();

            foreach ($result as $value) {
                $i = (int)$value["id"];
                $arr[$i] = json_decode(json_encode($value));
            }

            return $arr;
        }

        return false;
    }

    public static function _getReservesByCompanyId($companyId, $date)
    {
        $sql = "SELECT `cr`.`lastname` Nume, `cr`.`firstname` Prenume, `cr`.`reserve_date` `Data`, `cr`.`datetime_start` `Inceput`, `cr`.`duration` Durata  FROM companies_reserves cr INNER JOIN companies c ON `cr`.`company_id` = `c`.`id` WHERE `c`.`id` = $companyId AND `cr`.`reserve_date` = '$date'";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();
            $i = 1;
            foreach ($result as $value) {
                $arr[] = array_merge(array("N/o" => $i), $value);
                $i++;
            }
            return $arr;
        }

        return false;
    }

    public static function getWeekScheduleByCompanyId($companyId)
    {
        $sql = "SELECT
                    week_day id,
                    start_time,
                    stop_time,
                    p_start_time,
                    p_stop_time,
                    meet_duration
                FROM
                    companies_schedule
                WHERE
                    company_id = $companyId
                ORDER BY id ASC";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();

            foreach ($result as $value) {
                $i = (int)$value["id"];
                $arr[$i] = json_decode(json_encode($value));
            }

            return $arr;
        }

        return false;
    }

    public static function getAllActive()
    {
        $sql = "SELECT * FROM companies WHERE active = 1";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (count($result)) {
            $arr = array();

            foreach ($result as $row) {
                $arr[$row["id"]] = $row;
            }

            return $arr;
        }

        return true;
    }

    public static function addRegistration($companyId, $fields)
    {
        $sql = "INSERT INTO companies_reserves(company_id, datetime, reserve_date, datetime_start, duration, firstname, lastname, email_address, phone_number) VALUES ($companyId, '".App::now()."', '".$fields["reserve_date"]."', '".$fields["datetime_start"]."', ".$fields["duration"].", '".$fields["firstname"]."', '".$fields["lastname"]."', '".$fields["email_address"]."', '".$fields["phone_number"]."')";

        if (!App::$db->query($sql)) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return true;
    }

    public static function addContact($companyId, $fields)
    {
        $sql = "INSERT INTO companies_requests(company_id, datetime, firstname, lastname, email_address, phone_number, description) VALUES ($companyId, '".App::now()."', '".$fields["firstname"]."', '".$fields["lastname"]."', '".$fields["email_address"]."', '".$fields["phone_number"]."', '".$fields["description"]."')";

        if (!App::$db->query($sql)) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return true;
    }

    public static function _getAllRequestsByCompanyId($companyId, $where = "1=1")
    {
        $sql = "SELECT lastname Nume, firstname Prenume, email_address `Adresa email`, description `Descrierea problemei` FROM companies_requests WHERE company_id = $companyId AND $where";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        if (is_array($result) && count($result)) {
            $arr = array();
            $i = 1;
            foreach ($result as $value) {
                $arr[] = array_merge(array("N/o" => $i), $value);
                $i++;
            }
            return $arr;
        }

        return false;
    }

    public static function getByUserId($userId)
    {
        $sql = "select c.id, c.name, c.address, c.phone_number from companies c inner join companies_members cm on c.id = cm.company_id inner join users u on u.id = cm.user_id OR u.id = c.owner_id WHERE u.id = $userId";

        if (!($result = App::$db->query($sql))) {
            App::$log->error("SQL Error $sql");
            return false;
        }

        return $result;
    }
}


?>