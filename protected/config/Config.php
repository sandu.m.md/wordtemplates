<?php

class Config
{
	private $conf;

	function __construct()
    {
		global $baseDir;

		$this->conf = json_decode(json_encode(array(
			"debug" => true,

			"errors" => array(
				"show" => true,
				"file" => "store/error.log"
			),

			"template_files_dir" => "store/template_files/",

			"temporary_files_dir" => "store/temporary_files/",

			"requires" => array(),
			"css" => array(
				"dir" => "css",
				"files" => array("style.css")
			),

			"js" => array(
				"dir" => "js",
				"files" => array("main.js")
			),

			"db" => array(
				"host" => "127.0.0.1",
				"user" => "wdtpl",
				"pass" => "saniok2000",
				"dbname" => "wdtpl"
			),

			// "db" => array(
			// 	"host" => "remotemysql.com",
			// 	"user" => "6ZEjLeeXts",
			// 	"pass" => "yroXozN0fU",
			// 	"dbname" => "6ZEjLeeXts"
			// ),
			
			"session" => array(
				"duration" => "+ 25 minutes"
			),

			"pref"	=> array(
				"tpl" => "tpl_pref_index_"
			),

			"logger" => array(
				"error" => array(
					"enabled" => true,
					"file" => "store/err.log"
				),

				"request" => array(
					"enabled" => true,
					"file" => "store/req.log"
				)
			)

		)));
		
		foreach ($this->conf->css->files as $file) {
			$this->conf->requires[] = "<link rel=\"stylesheet\" href=\"". $this->conf->css->dir . "/$file" . "\">";
		}

		foreach ($this->conf->js->files as $file) {
			$this->conf->requires[] = '<script type="text/javascript" src="' . $this->conf->js->dir. "/$file" . '"></script>';
		}
	}
    
    public function __get($property)
    {
        return $this->conf->{$property};
    }
}


?>