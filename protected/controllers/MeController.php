<?php

class MeController
{
    public function index() {
        if (App::$session->isActive()) {
            Loader::load("Me");
        } else {
            App::redirect("/home");
        }
    }

    public function cab()
    {
        if (App::$session->isActive()) {
            $user = UsersModel::getById(App::$session->userId());
            $companies = CompaniesModel::getAllByUserId(App::$session->userId());
            Loader::load("Cabinet", array("user" => $user, "companies" => $companies));
        } else {
            App::redirect("/");
        }
    }

    public function getJuristConsultRequests()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                echo App::getJsonResponse(true, App::$db->getJuristConsultRequests(App::$session->userId()));
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Not logged in"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function sendJuristConsultResponse()
    {
        if (App::isPostrequest()) {
            if (App::$session->isActive()) {
                if (isset($_POST["response"]) && isset($_POST["request_id"])) {
                    if (App::$db->sendJuristConsultResponse(App::$session->userId(), $_POST["request_id"], $_POST["response"])) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse(false, false, array("message" => "Cannot send juristConsultResponse"));
                    }
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Missing input params"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Not logged in"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function test()
    {
        echo "Epta<br>";
        var_dump(ServicesModel::getAllActive("service_type = 'consult'"));
        echo "<br>Epta<br>";
    }

    public function profile()
    {
        $user = UsersModel::getById(App::$session->userId());

        Loader::load("MeProfile", array("user" => $user));
    }
}


?>