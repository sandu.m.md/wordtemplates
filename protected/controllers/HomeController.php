<?php

class HomeController
{
    public function index()
    {
        Loader::load("Home");
    }

    public function juristConsult()
    {
        if (App::isPostRequest()) {
            if (isset($_POST["problem"]) && isset($_POST["username"]) && isset($_POST["title"]) && isset($_POST["email"]) && $_POST["title"] != "" && $_POST["problem"] != "" && $_POST["email"] != "") {
                if (App::$db->insertJuristConsultRequest(App::$db->getUser($_POST["username"])["id"], $_POST["email"], $_POST["title"], $_POST["problem"])) {
                    echo App::getJsonResponse(true);       
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Db error"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Params are not setted"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function getJuristConsultServices()
    {
        if (App::isPostRequest()) {
            if ($result = App::$db->getJuristConsultServices()) {
                echo App::getJsonResponse(true, $result);
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Db error"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }
}


?>