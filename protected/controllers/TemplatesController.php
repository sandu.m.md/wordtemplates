<?php

class TemplatesController
{
    public function index()
    {
        Loader::load("Templates");
    }

    public function get()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                $word = isset($_POST["search"]) ? $_POST["search"] : "";
                $tpls = App::$db->getTemplatesByUserId(App::$session->userId(), $word);
    
                if (count($tpls)) {
                    echo App::getJsonResponse(true, $tpls, array("message" => "success"));
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "No templates"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "No active session"));
            }
        } else {
            App::redirect("/about");
        }
    }

    public function edit()
    {
        if (App::isPostRequest()) {

        } else {
            App::redirect("/home");
        }
    }

    public function remove()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                $docId = $GLOBALS["params"][0];

                if (DocsModel::deactivateById($docId)) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "A apărut o eroare"));
                }
            } else {
                App::getJsonResponse(false, false, array("message" => "Nu sunteți autentificat"));
            }
        } else {
            // App::redirect("/home");
        }
    }

    public function add()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                $target_file = App::$session->userId() . "_" . date("Y_m_d_H_i_s") . "_" . rand(100000, 999999) . ".docx";
    
                if (move_uploaded_file($_FILES["file"]["tmp_name"], App::$conf->template_files_dir . $target_file)) {
                    if ($tplId = App::$db->addTemplate($_POST["filename"], $target_file, App::$session->userId())) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse(false, false, array("message" => "Database error"));
                    }
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Cannot upload file"));
                }
            } else {
                echo App::getJsonResponse(false, json_encode(array("message"=>"Not connected!")));
            }
        } else {
            App::redirect("/home");
        }
    }

    public function getVariables()
    {
        App::loadPHPWord();

        $tplId = $_POST["templateId"];

        $tpl = App::$db->getTemplateById($tplId);

        $template = new \PhpOffice\PhpWord\TemplateProcessor(App::$conf->template_files_dir . $tpl["filename"]);
        
        $templateVariables = $template->getVariables();

        $res["templateId"] = array(
            "type" => "text",
            "style" => "display:none;",
            "value" => $tplId
        );

        foreach($templateVariables as $varName) {
            $res[$varName] = array(
                "type" => "text",
                "placeholder" => str_replace("_", " ", $varName)
            );
        }

        echo App::getJsonResponse(true, $res, array("templateName" => $tpl["name"]));
    }

    public function process()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {    

                $arr = array();
    
                foreach($_POST as $param) {
                    $param = json_decode($param);
                    $arr[$param->name] = $param->value;
                }
    
                App::loadPHPWord();
    
                $tpl = App::$db->getTemplateById($arr["templateId"]);
                $mainFilename = App::$conf->template_files_dir . $tpl["filename"];
    
                if (!file_exists($mainFilename)) {
                    echo App::getJsonResponse();
                    return;
                }
    
                $template = new \PhpOffice\PhpWord\TemplateProcessor(App::$conf->template_files_dir . $tpl["filename"]);  
    
                if (count($arr)) {
                    $template->setValue(array_keys($arr), array_values($arr));
                }
    
                $tempFilename = App::$conf->temporary_files_dir . $tpl["name"] . ".docx";
    
                if (file_exists($tempFilename)) {
                    unlink($tempFilename);
                }
    
                $template->saveAs($tempFilename);
    
                App::downloadFile($tempFilename);
            } else {
                App::errorLog(__FUNCTION__, "Not authenticated");
            }
        } else {
            App::redirect("/home");
        }
    }

    public function processDoc()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {    

                $arr = array();

                foreach($_POST as $param) {
                    $param = json_decode($param);
                    $arr[$param->name] = $param->value;
                }

                $tpl = end(DocsModel::getById($GLOBALS["params"][0]));

                $mainFilename = App::$conf->template_files_dir . $tpl["filename"];
    
                if (!file_exists($mainFilename)) {
                    echo App::getJsonResponse();
                    return;
                }

                App::loadPHPWord();
                $template = new \PhpOffice\PhpWord\TemplateProcessor(App::$conf->template_files_dir . $tpl["filename"]);  
    
                if (count($arr)) {
                    $template->setValue(array_keys($arr), array_values($arr));
                }
    
                $tempFilename = App::$conf->temporary_files_dir . $tpl["name"] . ".docx";
    
                if (file_exists($tempFilename)) {
                    unlink($tempFilename);
                }
    
                $template->saveAs($tempFilename);
    
                App::downloadFile($tempFilename);
            } else {
                App::errorLog(__FUNCTION__, "Not authenticated");
            }
        } else {
            // App::redirect("/");
        }
    }

    public function getVars()
    {
        

        $tplId = $_POST["templateId"];
        // $tplId = $GLOBALS["params"][0];

        // // echo ;
        // var_dump(DocsModel::getById($tplId));
        // exit();

        $tpl = DocsModel::getById($tplId)[$tplId];

        // var_dump()

        App::loadPHPWord();
        // $tpl = App::$db->getTemplateById($tplId);
        

        $template = new \PhpOffice\PhpWord\TemplateProcessor(App::$conf->template_files_dir . $tpl["filename"]);
        
        $templateVariables = $template->getVariables();

        $res["templateId"] = array(
            "type" => "hidden",
            "style" => "display:none;",
            "value" => $tplId
        );

        foreach($templateVariables as $varName) {
            $res[$varName] = array(
                "type" => "text",
                "placeholder" => str_replace("_", " ", $varName)
            );
        }

        echo App::getJsonResponse(true, $res, array("templateName" => $tpl["name"]));
    }

    public function getView()
    {
        $doc = end(DocsModel::getById($GLOBALS["params"][0]));

        $mainFilename = App::$conf->template_files_dir . $doc["filename"];

        include("lib/docxToHTML/docx_reader.php");

        $docx = new Docx_reader();
        $docx->setFile($mainFilename);

        if(!$docx->get_errors()) {
            $html = $docx->to_html();
            $plain_text = $docx->to_plain_text();

            echo $html;
        } else {
            echo implode(', ',$docx->get_errors());
        }
        echo "\n";
    }

    public function view()
    {
        $doc = end(DocsModel::getById($GLOBALS["params"][0]));

        $mainFilename = App::$conf->template_files_dir . $doc["filename"];

        // echo "<div style=\"width: calc(2.1 * 30vw); height: calc(2.97 * 30vw); padding: 40px; background-color: whitesmoke;\">";
        $this->readDocx($mainFilename);
        // echo "</div>";
    }

    private function readDocx($filePath) {
        // Create new ZIP archive
        $zip = new ZipArchive;
        $dataFile = 'word/document.xml';
        // Open received archive file
        if (true === $zip->open($filePath)) {
            // If done, search for the data file in the archive
            if (($index = $zip->locateName($dataFile)) !== false) {
                // If found, read it to the string
                $data = $zip->getFromIndex($index);
                // App::$log->error($data);
                // var_dump($data);
                // App::$log->error(json_encode($this->parseDocx($data)));
                // var_dump($this->parseDocx($data));
                $this->pars($data);
                // exit();
                // Close archive file
                $zip->close();
                // Load XML from a string
                // Skip errors and warnings
                $xml = DOMDocument::loadXML($data, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
                // Return data without XML formatting tags
    
                $contents = explode('\n',strip_tags($xml->saveXML()));
                $text = '';
                foreach($contents as $i=>$content) {
                    $text .= $contents[$i];
                }
                return $text;
            }
            $zip->close();
        }
        // In case of failure return empty string
        return "";
    }

    private function parseDocx($doc)
    {
        $word = "";
        // $xml = DOMDocument::loadXML($data)
        // $xml = simplexml_load_string($doc);
        preg_match("/<w:body>(.*)<\/w:body>/", (string)$doc, $preg);
        $doc = $preg[1];

        // var_dump($doc);

        // $test = "0123456789";

        // var_dump(substr($test, 0, -5));

        $ps = preg_split("/<\/w:p\s*[^>]*><w:p/", substr($doc, 0, -5));

        $ps[0] = preg_replace("/<w:p(|\s+[^>]*)>/", "", $ps[0]);

        $ps[count($ps) - 1] = preg_replace("/<\/w:p>/", "", $ps[count($ps) - 1]);
        

        // $ps[0] = preg_split("/<w:p\s*[^>]*>/", $ps[0])[1];

        // array_shift($ps);

        // preg_match_all("/<w:p[a-z0-9:\"=]*>([^<>]*)<\/w:p>/", $doc, $preg);
        // var_dump($doc);
        // var_dump($preg);

        // $ps = $preg;
        // $ps = array_map(function($val){return $val[1];}, $preg);
        // return false;
        // var_dump(count($ps));

        foreach($ps as $p) {
            $style="";

            preg_match("/<w:pPr>(.*)<\/w:pPr>/", $p, $preg);
            $pPr = $preg[1];


            $pPrs = preg_split("/\/><|><\//", substr($pPr, 1, strlen($pPr) - 2));
            
            foreach($pPrs as $pPr) {
                if (preg_match("/w:jc(.*)w:val=\"(.*)\"/", $pPr, $preg)) {
                    $style .= "text-align: $preg[2];";
                }
            }

            preg_split("/<w:r(|\s+[^>]*>(.*)/", $p, $preg);
            $rs =


            // alignment
            // preg_match("/<w:jc(.*)w:val=\"(.*)\"(.*)\/>/", $pPr, $preg);
            // var_dump($preg);

            $word .= "<p style=\"$style\">Epta</p>";
        }


        return $word;
        // return $xml->attributes();
    }

    function pars($txt) 
    {
        $xml = $this->xmlToArray(simplexml_load_string(str_replace("w:", "", $txt)));
        $body = $xml["document"]["body"];

        $html = "";

        $scale = 1.183;

        $w = "21";
        $h = "29.7";

        $width = $body["sectPr"]["pgSz"]["attributes"]["w"];
        $height = $body["sectPr"]["pgSz"]["attributes"]["h"];

        $top = $body["sectPr"]["pgMar"]["attributes"]["top"];
        $right = $body["sectPr"]["pgMar"]["attributes"]["right"];
        $bottom = $body["sectPr"]["pgMar"]["attributes"]["bottom"];
        $left = $body["sectPr"]["pgMar"]["attributes"]["left"];

        foreach($body["p"] as $p) {
            $style =  "";

            $in_tag_p = "";
            $style .= "width: 100%; text-align: " . $p["pPr"]["jc"]["attributes"]["val"] . ";";
            $style .= "margin: 0;";

            $sz = isset($p["pPr"]["rPr"]["sz"]["attributes"]["val"]) ? $p["pPr"]["rPr"]["sz"]["attributes"]["val"] : 22;
            $sz = (2.54 * $sz) / 144;
            $style .= "font-size: ".($sz*$scale)."cm";

            if (isset($p["r"]["1"])) {
                foreach($p["r"] as $r) {

                    $val = $r["t"]["value"];

                    if (preg_match_all("/{(\w+)}/", $val, $preg)) {
                        foreach($preg[1] as $key => $prg) {
                            if (isset($prg)) {
                                // $val = preg_replace("/{(\w+)}/", '[<span contenteditable="true">'.$prg.'</span>]', $val);
                                $sz = isset($p["pPr"]["rPr"]["sz"]["attributes"]["val"]) ? $p["pPr"]["rPr"]["sz"]["attributes"]["val"] : 22;

                                $sz = (2.54 * $sz) / 144;
                                $val = preg_replace("/{(\w+)}/", '<span style="font-size: '.($sz*$scale).'cm;">'.$prg.'</span>', $val);
                                // $val = preg_replace("/".$preg[0][$key]."/", '<input type="text" style="font-size: '.$sz.'cm;" name="'.$prg.'" placeholder="'.$prg.'">', $val);
                            }
                        }
                    }

                    $in_tag_p .= "<span>".$val."</span>";
                }

                $in_tag_p = "<p style=\"$style\">".$in_tag_p."</span>";
            } else {
                if (isset($p["r"])) {
                    $val = isset($p["r"]["t"]["value"]) ? $p["r"]["t"]["value"] : "<pre> </pre>";
                
                    if (preg_match_all("/{(\w+)}/", $val, $preg)) {
                        App::cLog("Epta");    
                        $val = preg_replace("/{(\w+)}/", '<input type="text" style="font-size: ' . (isset($p["pPr"]["rPr"]["sz"]["attributes"]["val"]) ? $p["pPr"]["rPr"]["sz"]["attributes"]["val"] : 22) . 'pt;" name="'.$preg[1].'" placeholder="'.$prg[1].'">', $val);
                    }

                    $in_tag_p .= "<p style=\"$style\">".$val."</span>";
                }
            }
            $html .= "$in_tag_p";
        }

        echo "<div style=\"\"><div style=\"
            box-sizing: border-box;
            background-color: whitesmoke;
            width: ".($w*$scale)."cm;
            height: ".($h*$scale)."cm;
            padding-top: ".($h * $top / $height * $scale)."cm;
            padding-right: ".($w * $right / $width * $scale)."cm;
            padding-bottom: ".($h * $bottom / $height * $scale)."cm;
            padding-left: ".($w * $left / $width * $scale)."cm;
        \">";
        echo $html;
        echo "</div></div>";
    }

    function xmlToArray(SimpleXMLElement $xml): array
    {
        $parser = function (SimpleXMLElement $xml, array $collection = []) use (&$parser) {
            $nodes = $xml->children();
            $attributes = $xml->attributes();

            if (0 !== count($attributes)) {
                foreach ($attributes as $attrName => $attrValue) {
                    $collection['attributes'][$attrName] = strval($attrValue);
                }
            }

            if (0 === $nodes->count()) {
                $collection['value'] = strval($xml);
                return $collection;
            }

            foreach ($nodes as $nodeName => $nodeValue) {
                if (count($nodeValue->xpath('../' . $nodeName)) < 2) {
                    $collection[$nodeName] = $parser($nodeValue);
                    continue;
                }

                $collection[$nodeName][] = $parser($nodeValue);
            }

            return $collection;
        };

        return [
            $xml->getName() => $parser($xml)
        ];
    }
}


function parse($txt)
{
    
    if (preg_match("/<w:body>(.*)<\/w:body>/", $txt, $preg)) {
        $txt = $preg[1];
    }
    
    $spl = preg_split("/>/", $txt);

    if (!count($spl)) {
        return $txt;
    }

    preg_match("/^<w:(\w+).*(\/)/", $spl[0], $preg);
    $tag = $preg[1];

    if (isset($preg[2])) {
        return $tag;
    } 

    $txt = preg_split("/<\/w:$tag>/", $txt)[0];


}

?>