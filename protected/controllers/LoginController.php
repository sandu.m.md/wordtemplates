<?php

class LoginController
{
    public function index()
    {
        if (!App::$session->isActive()) {
            Loader::load("Login");
        } else {
            App::redirect("/me/cab");
        }
    }

    public function auth()
    {
        if (App::isPostRequest()) {
            if (isset($_COOKIE["username"]) && isset($_COOKIE["password"])) {
                if (isset($_POST[$_COOKIE["username"]]) && isset($_POST[$_COOKIE["password"]])) {
                    if (App::$session->login($_POST[$_COOKIE["username"]], $_POST[$_COOKIE["password"]])) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse(false, false, array("message" => "Authentication failed"));
                    }
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Missing post data"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Missing name tag"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post requests"));
        }
    }
}

?>