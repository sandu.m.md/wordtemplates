<?php

// clasa pentru accesarea colectiilor de date

class ServicesController 
{
    public function consult()
    {
        echo App::getJsonResponse(true, ServicesModel::getFullAllActive());
    }

    public function sendRequest()
    {
        if (App::isPostRequest()) {
            if (isset($_POST["problem"]) && isset($_POST["service"]) && isset($_POST["title"]) && isset($_POST["email"]) && $_POST["title"] != "" && $_POST["problem"] != "" && $_POST["email"] != "") {
                if (ServicesRequestsModel::add($_POST["service"], $_POST["email"], $_POST["title"], $_POST["problem"])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Db error"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Missing input params"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function consultRequests()
    {
        if (App::isPostRequest()) {
            if ($result = ServicesRequestsModel::getAllByUserId(App::$session->userId())) {
                echo App::getJsonResponse(true, $result);
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Db error"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function sendServiceResponse()
    {
        if (App::isPostRequest()) {
            if (isset($_POST["response"]) && isset($_POST["request"])) {
                if (ServicesModel::isAllowedToUser(App::$session->userId(), ServicesRequestsModel::getById($_POST["request"])["company_service_id"])) {
                    if (ServicesRequestsModel::answer($_POST["request"], $_POST["response"])) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse(false, false, array("message" => "Db error"));
                    }
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Access restricted " . json_encode(ServicesRequestsModel::getById($_POST["request"])["company_service_id"])));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Missing input params"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function getCompanies()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                if ($result = CompaniesModel::getAllByUserId(App::$session->userId())) {
                    echo App::getJsonResponse(true, $result);
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Db error"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Not logged in"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function addCompany()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                if (isset($_POST["company_name"]) && isset($_POST["company_address"])) {
                    echo App::getJsonResponse(true, CompaniesModel::add(App::$session->userId(), $_POST["company_name"], $_POST["company_address"]));
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Db error"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Not logged in"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function addService()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                if (isset($_POST["company_id"]) && isset($_POST["service_type"]) && isset($_POST["service_name"]) && isset($_POST["service_description"])) {
                    if (CompaniesModel::isAllowedtoUser(App::$session->userId(), $_POST["company_id"])) {
                        if (ServicesModel::add($_POST["company_id"], $_POST["service_type"], $_POST["service_name"], $_POST["service_description"])) {
                            echo App::getJsonResponse(true);
                        } else {
                            echo App::getJsonResponse(false, false, array("message" => "Db error on insert"));
                        }
                    } else {
                        echo App::getJsonResponse(false, false, array("message" => "Access retricted"));
                    }
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Db error"));
                }
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Not logged in"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function getServices()
    {
        if (App::isPostRequest()) {
            if (App::$session->isActive()) {
                echo App::getJsonResponse(true, ServicesModel::getAllByUserId(App::$session->userId()));
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Not logged in"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Not post request"));
        }
    }

    public function getDocs()
    {
        if (!isset($GLOBALS["params"][0])) {
            echo App::getJsonResponse(false, false, array("message" => "Dea pula, nui service"));
            exit();
        }

        $serviceId = $GLOBALS["params"][0];

        echo App::getJsonResponse(true, ServicesModel::getDocsByServiceId($serviceId));
    }
}



?>