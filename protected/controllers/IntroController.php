<?php 

class IntroController
{
    public function index()
    {
        Loader::load("Intro");
    }

    public function companies()
    {
        Loader::load("Companies");
    }

    public function posts()
    {
        Loader::load("Posts");
    }

    public function about()
    {
        Loader::load("About");
    }
}

?>