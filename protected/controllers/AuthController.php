<?php

class AuthController
{
    public function login()
    {
        if (App::$session->login($_POST["username"], $_POST["password"])) {
            echo App::getJsonResponse(true);
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Login error"));
        }
    }

    public function register()
    {
        if (App::$db->createUser($_POST["username"], $_POST["password"])) {
            if (App::$session->login($_POST["username"], $_POST["password"])) {
                echo App::getJsonResponse(true);
            } else {
                echo App::getJsonResponse(false, false, array("message" => "Session login err"));
            }
        } else {
            echo App::getJsonResponse(false, false, array("message" => "Db err"));
        }
    }

    public function logout()
    {
        App::$session->logout();
        echo App::getJsonResponse(true);
    }

    public function out()
    {
        App::$session->logout();
        App::redirect("/");
    }

}

?>