<?php

class UtilController
{
    public function cal()
    {
        $companyId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;
        $year = isset($GLOBALS["params"][1]) ? $GLOBALS["params"][1] : (int)date("Y");
        $month = isset($GLOBALS["params"][2]) ? $GLOBALS["params"][2] : (int)date("m");

        $today = date("Y-m") == date("Y-m", strtotime("$year-$month-01")) ? (int)date("d") : false;

        $daysInMonth = (int)date("t", strtotime("$year-$month-01"));


        $cal = array();
        $weekOfMonth = 0;

        for($i = 1; $i <= $daysInMonth; $i++) {
            $dayOfWeek = (int)date("w", strtotime("$year-$month-$i"));
            $dayOfWeek = $dayOfWeek == 0 ? 7 : $dayOfWeek;
            $cal[$weekOfMonth][$dayOfWeek] = array("day_$year-$month-$i", "inactive");
            
            if ($today == $i) {
                array_push($cal[$weekOfMonth][$dayOfWeek], "today");
            }

            if ($dayOfWeek == 7) {
                $weekOfMonth++;
            }
        }

        // week schedule
        foreach (CompaniesModel::getWeekScheduleByCompanyId($companyId) as $weekDayNo => $sch) {
            foreach ($cal as $weekDayNo_ => $cl) {
                if (isset($cal[$weekDayNo_][$weekDayNo])) {
                    if ($today) {
                        if ($today <= (int)substr($cal[$weekDayNo_][$weekDayNo][0], 12, 2)) {
                            array_push($cal[$weekDayNo_][$weekDayNo], "active");
                        }
                    } else {
                        array_push($cal[$weekDayNo_][$weekDayNo], "active");
                    }
                }
            }
        }

        $bool = true;
        $num = count($cal);
        for($i = 0; $i < $num && $bool; $i++) {
            for($j = 1; $j < 8 && $bool; $j++) {
                if (is_array($cal[$i][$j])) {
                    if (in_array("active", $cal[$i][$j])) {
                        array_push($cal[$i][$j], "selected-day");
                        $bool = false;
                    }
                }          
            }
        }

        $resp = array(
            "month" => date("F", strtotime("$year-$month-01")),
            "year" => date("Y", strtotime("$year-$month-01")),
            "cal" => $cal
        );

        echo App::getJsonResponse(true, $resp);
    }

    public function cl()
    {
        $companyId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;
        $year = isset($GLOBALS["params"][1]) ? $GLOBALS["params"][1] : (int)date("Y");
        $month = isset($GLOBALS["params"][2]) ? $GLOBALS["params"][2] : (int)date("m");

        Loader::load("Cal", array("year"=>$year, "month"=>$month, "companyId"=>$companyId));
    }

    public function epta()
    {
        ?>
        <img src="res/banner-1.jpg" alt="Nu merge blet">
        <?php
    }

}


?>