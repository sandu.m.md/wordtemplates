<?php 

class CompaniesController 
{
    public function show()
    {
        
        if (!isset($GLOBALS["params"][0])) {
            echo "Dea pula";
            exit();
        }

        $companyId = $GLOBALS["params"][0];


        // var_dump(ServicesModel::getAllByCompanyId($companyId));
        // exit();

        Loader::load("CompanyShow", array("company" => CompaniesModel::getById($companyId), "services" => ServicesModel::getAllByCompanyId($companyId)));
        exit();
        if (App::$session->isActive()) {
            if (CompaniesModel::isAllowedtoUser(App::$session->userId(), $companyId)) {
                Loader::load("CompanyShow", array("company" => CompaniesModel::getById($companyId), "services" => ServicesModel::getAllByCompanyId($companyId)));
            } else {
                echo "Not allowed";
            }
        } else {
            echo "Not logged in";
        }
    }

    public function getAvailableReserves()
    {
        if (!isset($GLOBALS["params"][0]) || !isset($GLOBALS["params"][1]) || !isset($GLOBALS["params"][2]) || !isset($GLOBALS["params"][3])) {
            echo "Bad";
            exit();
        }

        $companyId = $GLOBALS["params"][0];
        $year = $GLOBALS["params"][1];
        $month = $GLOBALS["params"][2];
        $day = $GLOBALS["params"][3];

        $reserves = CompaniesModel::getReservesByCompanyId($companyId, "$year-$month-$day");
        $weekDay = (int)date("w", strtotime("$year-$month-$day"));
        $weekDay = $weekDay ? $weekDay : 7;
        $schedule = CompaniesModel::getWeekScheduleByCompanyId($companyId)[$weekDay];

        $start = strtotime($schedule->start_time);
        $stop = strtotime($schedule->p_start_time." - ".($schedule->meet_duration - 1)." minutes");
        $it = 0;
        for($i = $start; $i < $stop; $i = strtotime(date("H:i", $i) . " + ".$schedule->meet_duration." minutes")) {
            if (!isset($reserves[$it])) {
                $availableReserves[$it] = date("H:i", $i);
            }

            $it++;
        }

        $start = strtotime($schedule->p_stop_time);
        $stop = strtotime($schedule->stop_time." - ".($schedule->meet_duration - 1)." minutes");
        for($i = $start; $i < $stop; $i = strtotime(date("H:i", $i) . " + ".$schedule->meet_duration." minutes")) {
            if (!isset($reserves[$it])) {
                $availableReserves[$it++] = date("H:i", $i);
            }

            $it++;
        }

        echo App::getJsonResponse(true, $availableReserves);
    }

    public function getWeekSchedule()
    {
        if (!isset($GLOBALS["params"][0])) {
            echo "Dea pula";
            exit();
        }

        $companyId = $GLOBALS["params"][0];

        // var_dump(CompaniesModel::getWeekScheduleByCompanyId($companyId));
        echo App::getJsonResponse(true, CompaniesModel::getWeekScheduleByCompanyId($companyId));
    }

    public function index()
    {
        $companyId = 1;
        $company = CompaniesModel::getById($companyId);
        $services = ServicesModel::getAllByCompanyId($companyId);
        Loader::load("CompanyClient", array("company"=>$company, "services"=>$services));
    }

    public function client()
    {
        $companyId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 1;
        $company = CompaniesModel::getById($companyId);
        $services = ServicesModel::getAllByCompanyId($companyId);
        Loader::load("CompanyClient", array("company"=>$company, "services"=>$services));
    }

    public function memberview()
    {
        if (App::$session->isActive()) {
            if (isset($GLOBALS["params"][0])) {
                $companyId = $GLOBALS["params"][0];

                if (CompaniesModel::isAllowedtoUser(App::$session->userId(), $companyId)) {
                    $company = CompaniesModel::getById($companyId);
                    $services = ServicesModel::getAllByCompanyId($companyId);
                    Loader::load("CompanyMember", array("company"=>$company, "services"=>$services));
                } else {
                    echo "Nu aveți access";
                }
            } else {
                echo "Bad requests";
            }
        } else {
            App::redirect("/login");
        }
    }

    public function getAllActive()
    {
        echo App::getJsonResponse(true, CompaniesModel::getAllActive());
    }

    public function getDaySchedule()
    {
        $companyId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;
        $date = isset($_POST["date"]) ? $_POST["date"] : 0;

        $resp = array(
            "09:30",
            "11:00",
            "08:00",
            "13:15",
            "09:30",
            "11:00",
            "08:00",
            "13:15",
            "09:30",
            "11:00",
            "08:00",
            "13:15",
            "09:30",
            "11:00",
            "08:00",
            "13:15",
            "$date"
        );


        echo App::getJsonResponse(true, $resp);
    }

    public function register()
    {
        $companyId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

        // $date = $_POST["date"];
        // $start = $_POST["start"];
        // $duration = $_POST["duration"];
        // $firstname = $_POST["firstname"];
        // $lastname = $_POST["lastname"];
        // $email_address = $_POST["email_address"];
        // $phone_number = $_POST["phone_number"];

        if (CompaniesModel::addRegistration($companyId, $_POST)) {
            echo App::getJsonResponse(true);
        } else {
            echo App::getJsonResponse(false);
        }

    }

    public function contact()
    {
        $companyId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

        if (CompaniesModel::addContact($companyId, $_POST)) {
            echo App::getJsonResponse(true);
        } else {
            echo App::getJsonResponse(false);
        }
    }

    public function view1()
    {
        if (!isset($GLOBALS["params"][0])) {
            echo "<h1>dea pla<h2>";
        }

        $companyId = $GLOBALS["params"][0];

        Loader::load("CompanyV1", array("company"=>CompaniesModel::getById($companyId), "services"=>ServicesModel::getAllByCompanyId($companyId)));
    }

    public function getServices()
    {
        $companyId = $GLOBALS["params"][0];

        $data = ServicesModel::_getAllByCompanyId($companyId);
        
        if (is_array($data) && count($data)) {
            $fields = array_keys(end($data));
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));
        } else {
            echo App::getJsonResponse(true, true);
        }
        
    }

    public function getMembers()
    {
        $companyId = $GLOBALS["params"][0];

        $data = CompaniesModel::_getAllMembersByCompanyId($companyId);
        
        if (is_array($data) && $data) {
            $fields = array_keys(end($data));
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));
        } else {
            echo App::getJsonResponse(true, true);
        }
        
    }

    public function getDocs()
    {
        $companyId = $GLOBALS["params"][0];
        $serviceId = isset($GLOBALS["params"][1]) ? $GLOBALS["params"][1] : 0;

        if ($serviceId) {
            $data = DocsModel::_getAllByCompanyId($companyId, $serviceId);
        } else {
            $data = DocsModel::_getAllByCompanyId($companyId);
        }

        if (is_array($data) && count($data)) {
            $fields = array_keys(end($data));
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));
        } else {
            echo App::getJsonResponse(true, true);
        }
        
    }

    public function getReserves()
    {
        $companyId = $GLOBALS["params"][0];
     
        $year = isset($GLOBALS["params"][1]) ? $GLOBALS["params"][1] : date("Y");
        $month = isset($GLOBALS["params"][2]) ? $GLOBALS["params"][2] : date("m");
        $day = isset($GLOBALS["params"][3]) ? $GLOBALS["params"][3] : date("d");

        $data = CompaniesModel::_getReservesByCompanyId($companyId, "$year-$month-$day");
        
        if (is_array($data) && count($data)) {
            $fields = array_keys($data[0]);
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));
        } else {
            echo App::getJsonResponse(true, true);
        }
        
    }

    public function getRequests()
    {
        $companyId = $GLOBALS["params"][0];

        $data = CompaniesModel::_getAllRequestsByCompanyId($companyId);
        
        if (is_array($data) && count($data)) {
            $fields = array_keys(end($data));
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));   
        } else {
            echo App::getJsonResponse(true, true);
        }
        
    }

    public function getUnansweredRequests()
    {
        $companyId = $GLOBALS["params"][0];

        $data = CompaniesModel::_getAllRequestsByCompanyId($companyId, "answered=0");
        
        if (is_array($data) && count($data)) {
            $fields = array_keys(end($data));
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));   
        } else {
            echo App::getJsonResponse(true, true);
        }
        
    }

    public function getAnsweredRequests()
    {
        $companyId = $GLOBALS["params"][0];

        $data = CompaniesModel::_getAllRequestsByCompanyId($companyId, "answered=1");
        if (is_array($data) && count($data)) {
            $fields = array_keys(end($data));
            echo App::getJsonResponse(true, array("fields"=>$fields, "tableData"=>$data));   
        } else {
            echo App::getJsonResponse(true, true);
        }
    }

    public function getServicesView()
    {
        $company = CompaniesModel::getById($GLOBALS["params"][0]);
        Loader::load("CompanyServices", array("company" => $company));
    }

    public function getDocView()
    {
        $doc = end(DocsModel::getById($GLOBALS["params"][0]));

        // App::cLog(json_encode($doc));
        Loader::load("Doc", array("doc" => $doc));
    }
}


?>