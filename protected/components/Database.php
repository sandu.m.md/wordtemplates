<?php

class Database
{
    private $link;

    function __construct($db)
    {
        if (!($this->link = mysqli_connect($db->host, $db->user, $db->pass, $db->dbname))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_connect_error());
        }
    }

    public function api_install($sql)
    {
        mysqli_begin_transaction($this->link);

        if ($this->query($sql)) {
            mysqli_commit($this->link);
            return true;
        } else {
            mysqli_rollback($this->link);
            return false;
        }

    }

    public function query($sql)
    {
        if (!($result = mysqli_query($this->link, $sql))) {
            App::$log->error(mysqli_error($this->link));
            return false;
        }

        if ($result === true) {
            return true;
        }

        if (mysqli_num_rows($result) == 1) {
            $service = mysqli_fetch_assoc($result);
            return array($service["id"] => $service);
            // return $service;
        } elseif (mysqli_num_rows($result) > 1) {
            while($row = mysqli_fetch_assoc($result)) {
                $arr[] = $row;
            }

            return $arr;
        }

        return true;
    }

    public function getUser($username) 
    {
        if (!($result = mysqli_query($this->link, "SELECT * FROM `users` WHERE `username` = '$username'"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return mysqli_num_rows($result) ? mysqli_fetch_assoc($result) : false;
    }

    public function getUserById($id) {
        if (!($result = mysqli_query($this->link, "SELECT * FROM `users` WHERE `id` = $id"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return mysqli_num_rows($result) ? mysqli_fetch_assoc($result) : false;
    }

    public function getSessionById($sessionId)
    {
        if (!($result = mysqli_query($this->link, "SELECT * FROM `sessions` WHERE `id` = $sessionId LIMIT 1"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return mysqli_num_rows($result) ? mysqli_fetch_assoc($result) : false;
    }

    public function startSession($userId)
    {
        if (!mysqli_query($this->link, "INSERT INTO `sessions`(`user_id`, `start_datetime`, `stop_datetime`, `ip_address`, `active`) VALUES ($userId, '" . App::now() . "', '" . App::getSessionStopTime() . "', '" . App::getIp() . "', 1)")) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return mysqli_affected_rows($this->link) ? mysqli_insert_id($this->link) : false;
    }

    public function stopSession($sessionId)
    {
        if (!mysqli_query($this->link, "UPDATE templates SET active = 0 WHERE id = $sessionId")) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return true;
    }

    public function getTemplatesByUserId($userId, $word)
    {
        if (!($result = mysqli_query($this->link, "SELECT `ut`.`id`, `template_id`, `name`, `filename`, `created` FROM `users_templates` `ut` INNER JOIN `templates` `t` ON `ut`.`template_id` = `t`.`id` WHERE `user_id` = $userId AND `ut`.`active` = 1 AND `name` LIKE '%$word%'"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }
        
        if (!mysqli_num_rows($result)) {
            return false;
        }

        $templates = array();

        while($tmp = mysqli_fetch_assoc($result)) {
            $templates[$tmp["id"]] = $tmp;
        }
        
        return $templates;
    }

    public function getTemplateById($id) 
    {
        $sql = "SELECT `ut`.`id`, `name`, `filename`, `created` FROM `users_templates` `ut` INNER JOIN `templates` `t` ON `ut`.`template_id` = `t`.`id` WHERE `ut`.`id` = $id AND `ut`.`active` = 1";
        $sql = "SELECT id, name, filename from companies_services_docs where id = $id";

        if (!($result = mysqli_query($this->link, $sql))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return mysqli_fetch_assoc($result);
    }

    public function isTemplateAllowedToUser($templateId, $userId)
    {
        if (!($result = mysqli_query($this->link, "SELECT COUNT(id) as cnt FROM users_templates WHERE `template_id` = $templateId AND `user_id` = $userId"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        $result = mysqli_fetch_assoc($result);

        return $result["cnt"] == 1 ? true : false;
    }

    public function createUser($username, $password)
    {
        if (!(mysqli_query($this->link, "INSERT INTO `users`(`username`, `password`, `type_id`) VALUES ('$username', '" . md5($password) . "', 1)"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }   

        return mysqli_affected_rows($this->link) ? mysqli_insert_id($this->link) : false;
    }

    public function addTemplate($tplName, $tplFile, $tplUserId)
    {
        if (!(mysqli_query($this->link, "INSERT INTO templates(`created`, `name`, `filename`, `active`) VALUES ('" . App::now() . "', '$tplName', '$tplFile', 1)"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        if (!mysqli_affected_rows($this->link)) {
            return false;
        }

        if (!(mysqli_query($this->link, "INSERT INTO `users_templates`(`template_id`, `user_id`, `active`) VALUES (" . mysqli_insert_id($this->link) . ", " . $tplUserId . ", 1);"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }
        
        return mysqli_affected_rows($this->link) ? mysqli_insert_id($this->link) : false;
    }

    public function removeTemplate($tplId)
    {
        if (!(mysqli_query($this->link, "UPDATE `users_templates` SET `active` = 0 WHERE `id` = $tplId"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }
        
        return mysqli_affected_rows($this->link) ? true : false;
    }

    public function insertJuristConsultRequest($to_user_id, $client_email, $problem_title, $problem_text)
    {
        if (!mysqli_query($this->link, "INSERT INTO `jurist_consult_requests`(`to_user_id`, `datetime`, `client_email`, `title`, `text`) VALUE ($to_user_id, '".App::now()."', '$client_email', '$problem_title', '$problem_text');")) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        return mysqli_insert_id($this->link) ? true : false;
    }

    public function getJuristConsultRequests($user_id)
    {
        if (!($result = mysqli_query($this->link, "SELECT * FROM `jurist_consult_requests` WHERE `to_user_id` = $user_id AND `active` = 1"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        // App::$log->error("e", "e", "SELECT * FROM `jurist_consult_requests` WHERE `to_user_id` = $user_id");

        if (!mysqli_num_rows($result)) {
            return false;
        }

        $requests = array();

        while($tmp = mysqli_fetch_assoc($result)) {
            $requests[$tmp["id"]] = $tmp;
        }
        
        return $requests;
    }

    public function sendJuristConsultResponse($user_id, $request_id, $response_text)
    {
        if (!($result = mysqli_query($this->link, "SELECT COUNT(*) as `cnt` FROM `jurist_consult_requests` WHERE `to_user_id` = $user_id AND `id` = $request_id"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->link));
            return false;
        }

        if (!mysqli_fetch_row($result)[0]) {
            return false;
        }

        if (!(mysqli_query($this->link, "UPDATE `jurist_consult_requests` SET `response` = '$response_text', `responded` = 1, `active` = 0 WHERE `to_user_id` = $user_id AND `id` = $request_id"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->error));
            return false;
        }

        return mysqli_affected_rows($this->link) ? true : false;
    }

    public function getJuristConsultServices()
    {
        if (!($result = mysqli_query($this->link, "SELECT `jcs`.`id`, `u`.`username`, `jcs`.`title` FROM `jurist_consult_services` `jcs` INNER JOIN `users` `u` ON `jcs`.`user_id` = `u`.`id` WHERE `jcs`.`active` = 1"))) {
            App::$log->error(__CLASS__, __FUNCTION__, mysqli_error($this->error));
            return false;
        }
        
        if (!mysqli_num_rows($result)) {
            return false;
        }

        $services = array();

        while($tmp = mysqli_fetch_assoc($result)) {
            $services[$tmp["id"]] = $tmp;
        }
        
        return $services;
    }
}

?>