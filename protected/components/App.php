<?php

class App
{
    public static $conf;
    public static $log;
    public static $db;
    public static $session;
    public static $logFile;
    public static $errorLogFile;

    public static $respCode = 200;

    public static function init()
    {
        self::$conf = new Config();
        self::$log = new Logger(self::$conf->logger);
        self::$db = new Database(self::$conf->db);
        self::$session = new Session();

        self::$logFile = fopen("store/request.log", "a+");
        self::$errorLogFile = fopen(self::$conf->errors->file, "a+");
    }

    public static function redirect($url) {
        echo "<script>window.location.replace(\"$url\");</script>";
    }

    public static function getSessionStopTime()
	{
		return date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . " " . self::$conf->session->duration));
    }
    
    public static function cLog($text)
    {
        echo "<script>console.log('$text');</script>";
    }

    public static function now()
    {
        return date("Y-m-d H:i:s");
    }

    public static function loadPHPWord()
    {
        # use PHPWord
	    require_once("vendor/autoload.php");
    }

    public static function downloadFile($file)
    { 
        if (file_exists($file)) {
            $baseFile = end(explode("/", $file));
            ob_start();
            ob_flush();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $baseFile . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            if (readfile($file)) {
                unlink($file);
            }
            
            return true;
        } 

        return false;
    }

    public static function includeJs($file)
    {
        echo "<script>" . file_get_contents($file) . "</script>";
        // echo "<script src=\"$file\"></script>";
        // echo "<script type=\"text/javascript\" src=\"http://wordtemplates/$file\"></script>\n";
        // echo "<script type=\"text/javascript\" src=\"http://".$_SERVER['HTTP_HOST']."/$file\"></script>\n";
    }

    public static function includeCss($file)
    {
        echo "<style>" . file_get_contents($file) . "</style>";
        // echo "<link href=\"$file\">";
        // echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://wordtemplates/$file\" />\n";
        // echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://".$_SERVER['HTTP_HOST']."/$file\" />\n";
    }

    public static function getIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function log($msg)
    {
        fwrite(self::$logFile, self::now() . " " . self::getIp() . " " . $msg . "\n");
    }

    public static function errorLog($module, $msg)
    {
        if (self::$conf->errors->show) {
            fwrite(self::$errorLogFile, self::now() . " [" . $module . "]\t" . $msg . "\n");
        }
    }

    public static function getJsonResponse($ok = false, $body = false, $info = false)
    {
        return '{"ok":' . json_encode($ok) . ', "body": ' . json_encode($body) . ', "info":' . json_encode($info) . '}';
    }

    public static function isPostRequest()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public static function getLocation()
    {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
}


?>