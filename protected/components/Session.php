<?php

class Session
{
    private $active = false;
    private $id;
    private $username;
    private $userId;

    public function check()
    {
        if (!(isset($_SESSION) && isset($_SESSION["id"]))) {
            $this->active = false;
            return false;
        }

        if (!($session = App::$db->getSessionById($_SESSION["id"]))) {
            $this->active = false;
            return false;
        }

        if ($session["stop_datetime"] < App::now()) {
            $this->active = false;
            return false;
        }

        $this->username = $_SESSION["username"];
        $this->userId = $_SESSION["user_id"];
        $this->id = $_SESSION["id"];

        $this->active = true;
        return true;
    }

    public function login($username, $password) 
    {
        if (!($user = App::$db->getUser($username))) {
            $this->active = false;
            return false;
        }

        if ($user["password"] != md5($password)) {
            $this->active = false;
            return false;
        }

        if (!$this->start($user["id"], $user["username"])) {
            $this->active = false;
            return false;
        }

        $this->username = $user["username"];
        $this->userId = $user["id"];

        $this->active = true;
        return true;
    }

    public function logout()
    {
        App::$db->stopSession($this->id);
        session_destroy();
    }

    public function start($userId, $username)
    {
        if (!($this->id = App::$db->startSession($userId))) {
            $this->active = false;
            return false;
        }

        $_SESSION["id"] = $this->id;
        $_SESSION["username"] = $username;
        $_SESSION["user_id"] = $userId;

        $this->active = true;
        return true;
    }

    public function stop()
    {
        session_destroy();
    }

    public function isActive()
    {
        return $this->active;
    }

    public function username()
    {
        return $this->username;
    }

    public function userId()
    {
        return $this->userId;
    }

    public function id()
    {
        return $this->id;
    }
}

?>