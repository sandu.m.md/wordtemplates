<?php

class Logger
{
    private $conf;
    private $errorFile;
    private $requestFile;
    
    /**
    * @param conf => {"error":{"file":"*.log"}, "request":{"file":"*.log"}}
     */

    function __construct($conf)
    {
        $this->conf = $conf;

        $this->errorFile = @fopen($this->conf->error->file, "a+");
        $this->requestFile = @fopen($this->conf->request->file, "a+");
    }

    public function error($msg)
    {
        $class = debug_backtrace()[1]['class'];
        $method = debug_backtrace()[1]['function'];
        $msgs = trim(preg_replace('/\s\s+/', " ", $msg));

        if ($this->conf->error->enabled) {
            fwrite($this->errorFile, $this->getIp() . " " . date("Y-m-d H:i:s") . " [$class] [$method] [$msg]\n");
        }
    }

    public function request($user, $root, $respCode)
    {
        if ($this->conf->request->enabled) {
            fwrite($this->requestFile, $this->getIp() . " " . date("Y-m-d H:i:s") . " [$user] [$root] [$respCode]\n");
        }
    }

    private function getIp()
    {
        return App::getIp();
    }

    public function test()
    {
        var_dump(debug_backtrace()[1]['function']);
        var_dump(debug_backtrace()[1]['class']);
    }
}

?>