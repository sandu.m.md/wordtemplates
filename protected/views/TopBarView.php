<div class="bx-flex-row bx-txt-white">
    <a href="/" class="bx-flex-el bx-flex bx-content-center bx-txt-white bx-title-1 miw-2 pointer fp-0">NotarAsist</a>
    <div class="bx-flex-el bx-flex-row bx-full-w fp-0" top-bar>
        <a href="/intro" class="bx-flex-el bx-pad-t bx-pad-b bx-pad-l-2 bx-pad-r-2 top-btn fp-1" onclick="topBtnClick($(this));">Acasă</a>
        <a href="/intro/companies" class="bx-flex-el bx-pad-t bx-pad-b bx-pad-l-2 bx-pad-r-2 top-btn fp-1" onclick="topBtnClick($(this));">Notari</a>
        <a href="/intro/posts" class="bx-flex-el bx-pad-t bx-pad-b bx-pad-l-2 bx-pad-r-2 top-btn fp-1" onclick="topBtnClick($(this));">Publicații</a>
        <a href="/intro/about" class="bx-flex-el bx-pad-t bx-pad-b bx-pad-l-2 bx-pad-r-2 top-btn fp-1" onclick="topBtnClick($(this));">Despre</a>
        <!-- <div class="bx-flex-el bx-pad-t bx-pad-b bx-pad-l-2 bx-pad-r-2 top-btn fp-1" onclick="notif();">Despre</div> -->
    </div>
    <div class="bx-flex-el bx-flex-row-r">
        <a href="/login"><div class="bx-flex-el bx-pad-t bx-pad-b bx-pad-l-2 bx-pad-r-2 bx-txt-white top-btn fp-0">Autentificare</div></a>
    </div>
</div>
<script>
$("[top-bar] > .top-btn:nth-child("+'<?php echo $data->selected?>'+")").trigger("click");
// notif("A apărut o problemă");
</script>