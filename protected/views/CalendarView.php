<?php 
    // conf zone

    $year = "2019";
    $month = "11";
    $companyId = 1;

    $schedule = CompaniesModel::getWeekScheduleByCompanyId($companyId);

    // don't touch, it works
    $date = strtotime("$year-$month-01");
    $monthName = date("F", $date);
    $firstDayInWeek = (int)date("w", $date);
    $daysInMonth = (int)date("t", $date);
    $monthDay = 1;
    $today = date("Y-m", $date) != date("Y-m") ? 0 : (int)date("d");

    for($i = 0; $i < 35; $i++) {
        if ($i < 7 && $i < $firstDayInWeek - 1) {
            $cal[$i/7][$i%7+1] = 0;
        } elseif($monthDay > $daysInMonth) {
            $cal[$i/7][$i%7+1] = 0;
        } else {
            $cal[$i/7][$i%7+1] = $monthDay++;
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?Loader::load("HeadRequires");?>
    <?App::includeCss("css/calendar.css");?>
</head>
<body>
    <div class="wrapper">
        <div class="container centered top-padded" style="width: 350px; display: flex;">
            <div class="calendar">
                <div class="calendar-box">
                    <div class="calendar-box-month">
                        <!-- <div class="calendar-box-month-prev"><</div> -->
                        <div class="calendar-box-month-name"></div>
                        <!-- <div class="calendar-box-month-next">></div> -->
                    </div>
                    <div class="calendar-box-weekdays">
                        <span>Mo</span>
                        <span>Tu</span>
                        <span>We</span>
                        <span>Th</span>
                        <span>Fr</span>
                        <span>Sa</span>
                        <span>Su</span>
                    </div>
                    <div class="calendar-box-days"></div>
                </div>
                <!-- <div class="calendar-day"></div> -->
            </div>
        </div>
    </div>
    <script>
    $(".inactive").attr("title", "Is not available");
    $(".today").attr("title", "Today");
    $("[normal-day]").addClass("inactive");


    $("span[active]").click(function(e){
        $("span[active]").removeClass("selected");
        $(e.target).addClass("selected");
        var day = parseInt($(e.target).html());
        var cal = $(".calendar-day");
        cal.empty();

        $.post("companies/getAvailableReserves/<?echo$companyId?>/<?echo$year?>/<?echo$month?>/"+day, {}, function(data){
            try {
                var resp = JSON.parse(data);
                // console.log(data);
                if (resp.ok) {
                    if (resp.body !== false && resp.body !== true) {
                        $.each(resp.body, function(key, value) {
                            cal.append('<div class="calendar-day-item">At '+value+'</div>')
                        });
                    } else {
                        cal.append("Nu sunt disponibile");
                        console.log("Wrong body");
                    }
                } else {
                    console.log(resp.info.message);
                }
            } catch (e) {
                console.log(e.message);
                console.log(data);
            }
        });
    });

    // cal("2019/11");

    

    function _cal(date = "") {
        var dt = new Date(date+"/02");
        var prev = dt.getFullYear() + "/" + dt.getMonth();
        dt.setMonth(dt.getMonth() + 2);
        var next = dt.getFullYear() + "/" + dt.getMonth();
        
        dt.setMonth(dt.getMonth() - 2);

        $(".calendar-box-month-prev").click(function(e){
            console.log(1);
            $(e.target).click()
            cal(prev)

            
        });

        $(".calendar-box-month-next").click(function(e){
            console.log(2);
            cal(next);
        });

        cal(date);
    }

    // function _cal(date="") {
    //     if (date == "") {
    //         date = new Date().getFullYear + "/" + new Date().getMonth;
    //     }

    //     $.post("util/cal/"+date, {}, function(data) {
    //         var cal = $(".calendar-box-days");
    //         cal.empty();

    //         try {
    //             var resp = JSON.parse(data);
    //             if (resp.ok) {
    //                 $(".calendar-box-month-name").html(resp.body.month + " " + resp.body.year);

    //                 for(var weekOfMonth = 0; weekOfMonth < resp.body.cal.length; weekOfMonth++) {
    //                     cal.append('<div class="calendar-box-days-row"></div>');
    //                     var week = $(".calendar-box-days-row:last-child");

    //                     $(".calendar-box-month-prev").click(function(e){
    //                         console.log(1);
    //                         cal(resp.body.prevmonth);
    //                     });

    //                     $(".calendar-box-month-next").click(function(e){
    //                         console.log(2);
    //                         cal(resp.body.nextmonth);
    //                     });

    //                     for(var dayOfWeek = 1; dayOfWeek < 8; dayOfWeek++) {
    //                         if (resp.body.cal[weekOfMonth] != null && resp.body.cal[weekOfMonth][dayOfWeek] != null) {
    //                             week.append('<span ' + resp.body.cal[weekOfMonth][dayOfWeek].join(" ") + ' >' + parseInt(resp.body.cal[weekOfMonth][dayOfWeek][0].split("-")[1]) + '</span>');
    //                         } else {
    //                             week.append('<span hidden_day>0</span>');
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 console.log(resp.info.message);
    //             }
    //         } catch (ex) {
    //             console.log(ex.message);
    //             console.log(data);
    //         }
    //     });
    // }


    function cal(date = "") {
        if (date == "") {
            date = new Date().getFullYear + "/" + new Date().getMonth;
        }

        $.post("util/cal/"+date, {}, function(data) {
            var cal = $(".calendar-box-days");
            cal.empty();

            try {
                var resp = JSON.parse(data);
                if (resp.ok) {
                    $(".calendar-box-month-name").html(resp.body.month + " " + resp.body.year);

                    for(var weekOfMonth = 0; weekOfMonth < resp.body.cal.length; weekOfMonth++) {
                        cal.append('<div class="calendar-box-days-row"></div>');
                        var week = $(".calendar-box-days-row:last-child");

                        // $(".calendar-box-month-prev").click(function(e){
                        //     console.log(1);
                        //     cal(resp.body.prevmonth);
                        // });

                        // $(".calendar-box-month-next").click(function(e){
                        //     console.log(2);
                        //     cal(resp.body.nextmonth);
                        // });

                        for(var dayOfWeek = 1; dayOfWeek < 8; dayOfWeek++) {
                            if (resp.body.cal[weekOfMonth] != null && resp.body.cal[weekOfMonth][dayOfWeek] != null) {
                                week.append('<span ' + resp.body.cal[weekOfMonth][dayOfWeek].join(" ") + ' >' + parseInt(resp.body.cal[weekOfMonth][dayOfWeek][0].split("-")[1]) + '</span>');
                            } else {
                                week.append('<span hidden_day>0</span>');
                            }
                        }
                    }
                } else {
                    console.log(resp.info.message);
                }
            } catch (ex) {
                console.log(ex.message);
                console.log(data);
            }
        });

    }

    cal("2019/11");

    
    </script>
</body>
</html>