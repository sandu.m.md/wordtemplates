<div class="navbar">
    <div class="navbar-logo" id="navbar-logo">
        <div class="navbar-logo-item" link-main>
            Logo
        </div>
    </div>

    <div class="navbar-menu">
        <div class="navbar-menu-item" link-home>Acasă</div>
        <div class="navbar-menu-item" link-templates>Draft</div>
        <div class="navbar-menu-item" link-manager>Manager</div>
        <div class="navbar-menu-item" link-about>Despre</div>
    </div>
    <div class="navbar-user">
        <?if (App::$session->isActive()) { ?>
            <div class="navbar-user-item active">
                <div class="dropdown active">
                    <div class="dropbtn active">Salut, <?echo App::$session->username();?> &#x25BE;</div>
                    <div class="dropdown-content">
                        <div class="dropdown-item" link-my-cab>Cabinet</div>
                        <div class="dropdown-item" link-setts>Setări</div>
                        <div class="dropdown-item" link-logout>Ieși</div>
                    </div>
                </div>
            </div>
        <? } else { ?>
            <div class="navbar-user-item" link-login>Log in</div>
            <div class="navbar-user-item" link-register>Sign up</div>
        <? } ?>
    </div>
</div>
<script>
    if (getUrlParameter("msg") == "success") {
        alert("Operatiune cu success");
    }
</script>