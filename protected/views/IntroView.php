<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JuristConsult</title>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <?php Loader::load("HeadRequires");?>
    <?php App::includeCss("css/intro.css");?>
    <?php App::includeCss("css/modal.css");?>
    <?php App::includeCss("css/main.css");?>
    <?php App::includeJs("js/main.js");?>
    <?php App::includeJs("js/init.js");?>
    <?php App::includeCss("css/comp.css");?>
    <?php App::includeCss("css/box.css");?>
    <style>
    * {
        box-sizing: border-box;
    }

    .frame:first-child {
        flex: 0 1 45px;
    }
    
    a {
        text-decoration: none;
    }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="bx-flex-col bx-bg-intro hp-8 bx-pad-t-2">
            <div class="bx-flex-el fp-0"><?php Loader::load("TopBar", array("selected" => 1));?></div>    
            <div class="bx-flex-el bx-flex bx-pad bx-content-center bx-txt-white bx-title-1">
                <div class="bx-flex-row bx-pad fp-8">
                    <div class="bx-flex-el bx-flex bx-pad bx-content-center">
                        <img src="/res/balance.png" class="h-3">
                    </div>
                    <div class="bx-flex-el bx-flex-row bx-pad bx-content-center">
                        <div class="bx-flex-el bx-title-2">Asistență notarială la cea mai înaltă calitate</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bx">    
            <div class="grid">
                <div class="grid-content">
                    <div class="grid-el">Epta1</div>
                    <div class="grid-el">Epta2</div>
                    <div class="grid-el">Epta3</div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function(){
        start();
        // $("[top-bar] > .top-btn:first-child").trigger("click");
        getCompanies();
    });
    
    function getCompanies() {
        var container = $(".grid-content");

        $.post("/companies/getAllActive", {}, function(data) {
            try {
                var resp = JSON.parse(data);

                if (resp.ok) {
                    if (resp.body !== false && resp.body !== true) {
                        container.empty();

                        $.each(resp.body, function (key, value) {
                            var id = makeId(15);

                            container.append('<div href="/companies/client/'+key+'" class="grid-el">'
                                            + '<div class="grid-el-title bx-bg-blue">'+value.name+'</div>'
                                            + '<div class="grid-el-content">'
                                            + '<div>Adresa: '+value.address+'</div>'
                                            + '<div>Tel: '+value.phone_number+'</div>'
                                            + '</div>'
                                            + '<a href="/companies/view1/'+key+'"><div class="grid-btn-next" id="'+id+'">&#8250;</div></a>'
                                            + '</div>');
                        });
                    } else {
                        console.log("Empty body");
                    }
                } else {
                    console.log(resp.info.message);
                }
            } catch (ex) {
                console.log(ex.message);
                console.log(data);
            }
        });
    }
    </script>
</body>
</html>