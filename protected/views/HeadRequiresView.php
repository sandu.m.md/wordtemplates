<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1" name="viewport" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-ajax-downloader@1.1.0/src/ajaxdownloader.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php 

App::includeCss("css/main.css");
App::includeCss("css/samples.css");
App::includeCss("css/navbar.css");
App::includeCss("css/modal.css");
App::includeCss("css/media.css");
App::includeCss("css/banner.css");
App::includeCss("css/dropdown.css");
App::includeCss("css/menu.css");
App::includeCss("css/company.css");
App::includeCss("css/buttons.css");
App::includeCss("css/box.css");
App::includeCss("css/calendar.css");

App::includeJs("js/main.js");
App::includeJs("js/init.js");
App::includeJs("js/template.js");
App::includeJs("js/epta.js");
App::includeJs("js/mycab.js");
App::includeJs("js/home.js");
App::includeJs("js/views.js");

?>
<script>
    $(document).ready(function(){
        start();
    });
</script>