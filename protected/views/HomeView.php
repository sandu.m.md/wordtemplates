<!DOCTYPE html>
<html>
<head>
	<title>Acasa</title>
	<?php Loader::load("HeadRequires");?>
</head>
<body>
	<?php Loader::load("Navbar");?>
	<div class="wrapper">
		<div class="container top-padded centered w-8 top-margin">
			<hr>
			<div class="container w-6 centered" list-juristConsultServices></div>
		</div>
	</div>
	<script>

		// getJuristConsultServices();
		getCompaniesServices();
		$("#jurist-help").click(function(e){
			$("#jurist-help").children("div.banner-body").show();
		});

		$("#jurist-help-submit").click(function(e){
			doAction({
				type: "request", 
				url: "/home/juristConsult", 
				ok: {
					type: "alert",
					message: "eblan"
					// type: "redirect",		
					// url: "/home"
				},
				notok: {
					type: "alert",
					message: "nunah"
				}
			}, "jurist-help-form");
		});



		$("div.dd-frame").click(function(e){
			var dd = $("#"+e.currentTarget.id);

			if (dd.children("div.dd-body").is(":hidden")) {
				if (dd.hasClass("dd-user"))$("div.dd-user").css({"position": "absolute"});
				else $("div.dd-user").css({"position": "static"});
				$("div.dd-body").hide();
				$("div.dd-frame").children("div.dd-title").removeAttr("style");

				dd.children("div.dd-title").css({"font-size": (parseInt(dd.children("div.dd-title").css("font-size"))+10)+"px"});
				dd.children("div.dd-body").show();
				dd.children("div.dd-body").children("form").children("input[type=text]").focus();
			}
		});

		$("#home-link").addClass("active");
		$("#need-help-btn").click(function(e){
			$("#need-help-info").show();
			$("#need-help-btn").css({"font-size": "27px", "background-color": "transparent"});
			$("#need-help").css({"padding": "15px"});
			$("#need-help-close-x").click(function(e){
				$("#need-help-info").hide();
				$("#need-help-btn").css({"font-size": "21px"});
				$("#need-help").css({"padding": "0px"});
			});
		});
	</script>
</body>
</html>