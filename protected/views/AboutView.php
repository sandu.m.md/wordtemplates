<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Despre</title>
    <?php Loader::load("HeadRequires")?>
</head>
<body>
    <div class="wrapper bx-flex-col mih-10">
        <div class="bx-flex-el bx-flex-col bx-pad-t-2 bx-bg-intro h-6 fp-0">
            <div class="bx-flex-el fp-0"><?php Loader::load("TopBar", array("selected" => 4));?></div>    
            <div class="bx-flex-el bx-flex bx-pad bx-content-center bx-txt-white bx-title-1">
                <div class="bx-flex-row bx-pad fp-0">
                    <div class="bx-flex-el bx-flex bx-pad bx-content-center bx-align-r">
                        <img src="/res/about.png" class="h-3">
                    </div>
                    <div class="bx-flex-el bx-flex-row bx-pad bx-content-center">
                        <div class="bx-flex-el bx-title-2 bx-txt-special bx-txt-special"><pre class="bx-txt-special">Despre noi</pre></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>