<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $data->company->name;?></title>
    <?php Loader::load("HeadRequires"); 
    App::includeCss("css/comp.css");
    App::includeCss("css/CompanyV1.css");
    App::includeCss("css/intro.css");?>
    <style>
    .wrapper {
        text-align: center;
        min-height: 100vh;
        height: 100%;
    }

    #day-sch {
        height: 25em;
    }

    </style>
</head>
<body>
    <div class="wrapper bx-bg-intro bx-flex-col">
        <div class="frame bx-flex bx-bg-intro bx-pad-t fp-0">
            <?php Loader::load("TopBar", array("selected" => 0))?>
        </div>
        <div class="frame">
            <div class="viewarea">
                <div class="btn-prev-next prev-frame-btn">&#8249;</div>
                <div class="btn-prev-next next-frame-btn">&#8250;</div>
                <div class="lent">
                    <div class="lent-frame">
                        <div class="lent-frame-box bx-flex bx-pad-4 h-9">
                            <div class="bx-full bx-flex fp-10 bx-bg-white">
                                <div class="bx-flex bx-full bx-flex-col bx-shadow">
                                    <div class="company-name bx-flex-el bx-title-1 bx-pad fp-0"><?php echo $data->company->name;?></div>
                                    <div class="bx-flex-col bx-flex-el bx-pad bx-bg-whitesmoke">
                                        <div class="bx-flex-el bx-pad fp-0">
                                            <div class="bx-title-3">Alegeti categoria și serviciul de care aveți nevoie</div>
                                        </div>
                                        <div class="bx-flex-el bx-flex-row fp-0">
                                            <div class="bx-flex bx-flex-el bx-pad bx-content-center fp-5">
                                                <select name="doc_category">
                                                    <option value="#">Selectare categorie document</option>
                                                    <?php foreach ($data->services as $servId => $serv) {?>
                                                        <option value="<?php echo $servId;?>"><?php echo $serv->title;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="bx-flex bx-flex-el bx-pad bx-content-center fp-5">
                                                <select name="doc_name">
                                                    <option value="#">Selectare document</option>
                                                    <?php foreach ($data->services as $servkey => $serv) {
                                                        foreach ($serv->docs as $dockey => $doc) {?>
                                                            <option value="<?php echo $dockey?>"><?php echo $doc->name?></option>
                                                    <?php }}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="bx-flex bx-pad bx-flex-el">
                                            <div class="bx-flex bx-flex-el bx-full-w">
                                                <div class="bx-align-b bx-full-w">
                                                    <div class="bx-flex bx-flex-row-dir bx-aligh-r">
                                                        <div class="btn btn-simple" onclick="next();">Pasul următor</div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lent-frame">
                        <div class="lent-frame-box bx-flex bx-pad-2">
                            <div class="bx-flex bx-full fp-10">
                                <div class="bx-flex-col bx-full-w bx-bg-whitesmoke bx-shadow">
                                    <div class="bx-flex bx-flex-el bx-pad company-name bx-title-2 fp-0">Pasul 2</div>
                                    <div class="bx-flex-el bx-flex bx-pad-t bx-content-center fp-0 bx-title-3" id="meeting-info">Alegeți data și ora la care puteți veni</div>
                                    <div class="bx-flex-el bx-flex-col">
                                        <div class="bx-flex-el bx-flex-row fp-0">
                                            <div class="bx-flex-el bx-pad fp-0">
                                                <div class="calendar">
                                                    <div class="calendar-box">
                                                        <div class="calendar-box-month">
                                                            <div class="calendar-box-month-prev">&#8249;</div>
                                                            <div class="calendar-box-month-name"></div>
                                                            <div class="calendar-box-month-next">&#8250;</div>
                                                        </div>
                                                        <div class="calendar-box-weekdays">
                                                            <span>Mo</span><span>Tu</span><span>We</span><span>Th</span><span>Fr</span><span>Sa</span><span>Su</span>
                                                        </div>
                                                        <div class="calendar-box-days"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bx-flex-el bx-dir-inversed bx-pad" id="day-sch"></div>
                                        </div>
                                        <div class="bx-flex bx-pad bx-flex-el bx-bg-whitesmoke">
                                            <div class="bx-flex bx-flex-el bx-full-w">
                                                <div class="bx-align-b bx-full-w">
                                                    <div class="bx-flex bx-flex-row-dir bx-aligh-r">
                                                        <div class="btn btn-simple" onclick="next();">Pasul următor</div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lent-frame">
                        <div class="lent-frame-box bx-pad-2 mih-7">
                            <div class="bx-flex bx-full bx-flex-col bx-bg-whitesmoke">
                                <div class="bx-flex-el bx-pad bx-bg-blue bx-txt-white bx-title-2 fp-0">Pasul 3</div>
                                <div class="bx-flex-el bx-pad">
                                    <form class="reg-form" name="doc_form">
                                        <div class="grid"></div>
                                    </form>
                                </div>
                                <div class="bx-flex bx-pad bx-flex-el bx-bg-whitesmoke fp-0">
                                    <div class="bx-flex bx-flex-el bx-full-w">
                                        <div class="bx-align-b bx-full-w">
                                            <div class="bx-flex bx-flex-row-dir bx-aligh-r">
                                                <div class="btn btn-simple" onclick="next();">Pasul următor</div>
                                            </div>
                                        </div>
                                    </div>                                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lent-frame">
                        <div class="lent-frame-box">
                            <div class="reg-box">
                                <div class="reg-title">Pasul 3</div>
                                <hr>
                                <br>
                                <div class="t1">Completati cu datele necesare</div>
                                <form class="reg-form" name="doc_form">
                                    <!-- <div class="grid">
                                    </div> -->
                                </form>
                                <div class="clearfix padded margined">
                                    <div class="btn right" onclick="next()">Continua</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lent-frame">
                        <div class="lent-frame-box">
                            <div class="reg-box">
                                <div class="reg-title">Cererea dumneavoastra a fost inregistrata cu success!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(".lent").css("width", "900vw");
        var currentFrame = 0;
        var totalFrames = 3;
        var frameWidth = parseInt($(".lent-frame").css("width")); 
        var frameMarginError = Math.abs(frameWidth - parseInt($(".viewarea").css("width"))) / 2;    

        toCurrentFrame();    
        // $("select[name=doc_name]").val(1)
        getDocsFields();

        function next() {
            if (currentFrame < totalFrames) {
                currentFrame++;
                toCurrentFrame();
            }
        }

        function prev() {
            if (currentFrame > 0) {
                currentFrame--;
                toCurrentFrame();
            }
        }

        function toCurrentFrame() {
            if (currentFrame > 0) {
                if ($("select[name=doc_name]").val() == "#") {
                    currentFrame = 0;
                    toCurrentFrame();
                    alert("Completați câmpurile");
                    return false;
                }
            }
            
            if (currentFrame == 0) {
                $(".prev-frame-btn").hide();
                $(".next-frame-btn").hide();
            } else if (currentFrame == totalFrames) {
                $(".prev-frame-btn").hide();
                $(".next-frame-btn").hide();
            } else {
                $(".prev-frame-btn").show();
                $(".next-frame-btn").hide();
            }

            

            if (currentFrame == 2) {
                getDocsFields();
            }

            $(".lent").css("left", (-1 * (frameWidth * currentFrame + frameMarginError)) + "px");
        }
        
        $(".next-frame-btn").click(function(e){
            next();
        });    
        
        $(".prev-frame-btn").click(function(e){
            prev();
        });
        

        var date = new Date();
        dt = date.getFullYear() + "/" + (date.getMonth() + 1);
        cal(dt, <?php echo $data->company->id;?>, false, true);

        $(".calendar-box-month-next").click(function(){
            date.setMonth(date.getMonth()+1);
            dt_ = date.getFullYear() + "/" + (date.getMonth() + 1);
            cal(dt_, <?php echo $data->company->id;?>, true, true);
        })

        $(".calendar-box-month-prev").click(function(){
            date.setMonth(date.getMonth()-1);
            dt_ = date.getFullYear() + "/" + (date.getMonth() + 1);
            if (dt_ > dt) {
                cal(dt_, <?php echo $data->company->id;?>, true, true);
            } else {
                cal(dt_, <?php echo $data->company->id;?>, false, true);
            }
        })

        $("select[name=doc_category]").change(function(e){
            var servId = $(e.target).val();
            if (servId == "#") {
                var el = $("select[name=doc_name]");
                el.empty();
                el.append('<option value="#">Selectare document</option>');

                $.each($(e.target).children("option"), function(key, opt){
                    if (key > 0)
                    $.post("/services/getDocs/"+$(opt).val(), {}, function(data){
                        try {
                            var resp = JSON.parse(data);

                            if (resp.ok) {
                                if (resp.body !== true && resp.body !== false) {
                                    $.each(resp.body, function(dockey, doc) {
                                        el.append('<option value="'+dockey+'">'+doc.name+'</option>');
                                    });
                                } else {
                                    console.log("Empty body");
                                }
                            } else {
                                console.log(resp.info.message);
                            }
                        } catch (e) {
                            console.log(e.message);
                            console.log(data);
                        }
                    });
                })
            } else {
                $.post("/services/getDocs/"+servId, {}, function(data){
                    try {
                        var resp = JSON.parse(data);

                        if (resp.ok) {
                            if (resp.body !== true && resp.body !== false) {
                                var el = $("select[name=doc_name]");
                                el.empty();
                                el.append('<option value="#">Selectare document</option>');

                                $.each(resp.body, function(dockey, doc) {
                                    el.append('<option value="'+dockey+'">'+doc.name+'</option>');
                                });
                            } else {
                                console.log("Empty body");
                            }
                        } else {
                            console.log(resp.info.message);
                        }
                    } catch (e) {
                        console.log(e.message);
                        console.log(data);
                    }
                });
            }
        });

        function getDocsFields() {
            var dockey = $("select[name=doc_name]").val();
            var form = $("form[name=doc_form]").children(".grid");

            $.post("/templates/getVars", {templateId: dockey}, function(data){
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (resp.body !== false && resp.body !== true) {
                            form.empty();

                            $.each(resp.body, function(fname, fval) {
                                fieldsHtml = '<input class="doc_input" name="' + fname + '"';

                                for(const[attrName, attrValue] of Object.entries(fval)) {
                                    fieldsHtml += ' ' + attrName + '="' + attrValue + '"';
                                }

                                fieldsHtml += '>';

                                form.append(fieldsHtml);
                            });
                        } else {
                            console.log("Empty body");
                        }
                    } else {
                        console.log("Bad resp");
                    }
                } catch (ex) {
                    console.log(ex.message);
                    console.log(data);
                }
            });
        }
    </script>
</body>
</html>