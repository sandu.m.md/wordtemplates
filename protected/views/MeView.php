<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profilul meu</title>
    <?php Loader::load("HeadRequires");?>
</head>
<body>
    <?php Loader::load("Navbar");?>
    <div class="wrapper">
        <table class="w-9 centered top-padded">
            <tr>
                <td colspan="3">
                    <div class="banner">
                        <span>Cabinetul personal</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr>
                </td>
            </tr>
            <tr>
                <td class="wp-2" rowspan="2">
                    <div class="menu box-bg">
                        <div class="menu-title">Menu</div>
                        <div class="menu-item">
                            <div class="menu-item-ico btn-add">&#10010;</div>
                            <div class="menu-item-title">New company</div>
                        </div>
                        <div class="menu-item">
                            <div class="menu-item-ico btn-add">&#10010;</div>
                            <div class="menu-item-title">Services</div>
                        </div>
                    </div>
                </td>
                <td class="wp-6" rowspan="2">
                    <div list-juristConsultRequests></div>
                </td>
                <td class="wp-2">
                    <div class="menu box-bg" menu-companies>
                    </div>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                    <div class="menu box-bg" menu-services>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <script>
    
    getCompaniesMenu();
    getServicesMenu();
    getConsultRequests();
    
    
    
    </script>
</body>
</html>