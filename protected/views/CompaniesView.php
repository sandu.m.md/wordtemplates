<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notari</title>
    <?php Loader::load("HeadRequires")?>
    <?php App::includeCss("css/intro.css");?>
    <style>
    * {
        box-sizing: border-box;
    }

    a {
        text-decoration: none;
    }
    </style>
</head>
<body>
    <div class="wrapper bx-flex-col mih-10">
        <div class="bx-flex-el bx-flex-col bx-pad-t-2 bx-bg-intro h-6 fp-0">
            <div class="bx-flex-el fp-0"><?php Loader::load("TopBar", array("selected" => 2));?></div>    
            <div class="bx-flex-el bx-flex bx-pad bx-content-center bx-txt-white bx-title-1">
                <div class="bx-flex-row bx-pad fp-8">
                    <div class="bx-flex-el bx-flex bx-pad bx-content-center">
                        <img src="/res/hammer.png" class="h-3">
                    </div>
                    <div class="bx-flex-el bx-flex-row bx-pad bx-content-center">
                        <div class="bx-flex-el bx-title-2 bx-txt-special">Asociațiile de notari disponibile</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bx-flex-el bx-pad bx-flex bx-bg-whitesmoke bx-content-center">
            <div class="bx-grid w-8" grid-companies>
                
            </div>
        </div>
        <div class="bx-flex-el bx-pad-2 bx-bg-dark bx-txt-white fp-0 bx-flex-row bx-txt-center">
            <div class="bx-flex-el bx-txt-center">Footer</div>
        </div>
    </div>
    <script>
    $(document).ready(function(){
        getCompanies();
    });

    function getCompanies() {
        var container = $("[grid-companies]");

        $.post("/companies/getAllActive", {}, function(data) {
            try {
                var resp = JSON.parse(data);

                if (resp.ok) {
                    if (resp.body !== false && resp.body !== true) {
                        container.empty();

                        $.each(resp.body, function (key, value) {
                            var id = makeId(15);

                            container.append('<div class="bx-grid-el bx-grid-el-sm bx-mg bx-shadow bx-bg-company bx-brd-circle bx-txt-white">'
                                            + '<div class="bx-flex-col bx-full bx-pad-t-2 bx-pad-b">'
                                            + '<div class="bx-flex-el bx-bg-dark bx-pad he-2 bx-scroll-h"><span class="bx-title-1 bx-txt-thin">'+value.name+'</span></div>'
                                            + '<div class="bx-flex-el bx-flex-col bx-pad-t bx-pad-l bx-pad-r bx-txt-center">'
                                            + '<div class="bx-flex-el fp-0 bx-pad-b">'+value.address+'</div>'
                                            + '<div class="bx-flex-el fp-2 bx-pad-b">'+value.phone_number+'</div>'
                                            + '</div>'
                                            + '<a href="/companies/view1/'+key+'" class="bx-flex-el bx-bg-white bx-txt-black bx-pad fp-0 btn-lg">Info</a>'
                                            + '</div>'
                                            + '</div>');

                            if(false)
                            container.append('<div class="grid-el">'
                                            + '<div class="grid-el-title bx-bg-blue">'+value.name+'</div>'
                                            + '<div class="grid-el-content">'
                                            + '<div>Adresa: '+value.address+'</div>'
                                            + '<div>Tel: '+value.phone_number+'</div>'
                                            + '</div>'
                                            + '<a href="/companies/view1/'+key+'"><div class="grid-btn-next" id="'+id+'">&#8250;</div></a>'
                                            + '</div>');
                        });
                    } else {
                        console.log("Empty body");
                    }
                } else {
                    console.log(resp.info.message);
                }
            } catch (ex) {
                console.log(ex.message);
                console.log(data);
            }
        });
    }
    </script>
</body>
</html>