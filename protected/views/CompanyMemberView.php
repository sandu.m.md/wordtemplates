<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?Loader::load("HeadRequires");?>
    <?App::includeCss("css/comp.css");?>
    <style>
        * {
            box-sizing: border-box;
        }
        table, td, th {
            border: 1px solid rgba(0, 0, 0, 0.3);
            border-collapse: collapse;
            padding: 10px;
            font-size: 1em;
            text-align: left;
        }

        .wrapper {
            display: flex !important;
        }

        table {
            width: 100%;
        }

        td:first-child {
            width: 1em;
        }

        th:first-child {
            width: 1em;
        }

        tr:nth-child(even) {
            background-color: whitesmoke;
        }

        tr:first-child {            
            font-size: 1.2em; 
        }

        a {
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="wrapper bx-scroll-h">
        <div class="company-manager-box bx-scroll-h" win>
            <div class="bx-flex bx-bg-whitesmoke bx-shadow sz-viewarea fp-10">
                <div class="bx-flex bx-flex-col fp-10">
                    
                    <div class="bx-flex-el bx-flex-col bx-bg-blue fp-0">
                        <div class="bx-flex-el fp-0 bx-bg-dark bx-flex-row bx-pad-05">
                            <div class="bx-flex-el fp-0 btn-sq bx-title-4 btn-sq-sm" btn-min-max>
                                <span class="deg-5">&#10141;</span>
                            </div>
                            <div class="bx-flex-el"></div>
                            <div class="bx-flex-el fp-0">
                                <a href="/me/cab" class="bx-link-white bx-single-line">Cabinet personal</a>
                            </div>
                        </div>
                        <div class="bx-flex-el bx-pad bx-title-1 bx-txt-center bx-txt-white">
                            <span><?echo$data->company->name?></span>
                        </div>
                    </div>
                    <div class="bx-flex bx-flex-el">
                        <div class="bx-flex bx-flex-col fp-10">
                            <div class="bx-flex-el fp-0">
                                <div class="bx-flex-row">
                                    <div class="bx-flex-el btn" m-btn-general>General</div>
                                    <div class="bx-flex-el btn" m-btn-program>Programări</div>
                                    <div class="bx-flex-el btn" m-btn-drafts>Drafts</div>
                                    <div class="bx-flex-el btn" m-btn-requests>Requests</div>
                                </div>
                            </div>
                            <div class="bx-flex bx-flex-el">
                                <div class="bx-flex bx-flex-row fp-10">
                                    <div class="bx-flex-el fp-2">
                                        <div class="bx-flex-col" left-menu>
                                        </div>
                                    </div>
                                    <div class="bx-flex bx-flex-el bx-pad bx-brd-l bx-bg-white">
                                        <div class="bx-flex-el bx-scroll-y-n h-5 bx-full-w" table-area>
                                            <div class="table">
                                                <table id="table">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    
    $(document).ready(function(){
        $("[m-btn-general]").trigger("click");

        if (getCookie("memberview") == "maximize") {
            $("[btn-min-max]").trigger("click");
        }
    })
    
    var lmenu = $("[left-menu]");

    $("[m-btn-general]").click(function(e){
        btnClick($(e.target));

        lmenu.empty();
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getServices/<?php echo $data->company->id;?>\');btnClick($(this))">Services</div>');
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getMembers/<?php echo $data->company->id;?>\');btnClick($(this))">Members</div>');
        lmenu.children(".btn:first-child").trigger("click");
    });

    $("[m-btn-program").click(function(e) {
        btnClick($(e.target));
        
        lmenu.empty();
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getReserves/<?php echo $data->company->id;?>\');btnClick($(this))">Rezervări</div>');
        lmenu.children(".btn:first-child").trigger("click");
    });


    $("[m-btn-drafts]").click(function(e){
        btnClick($(e.target));

        lmenu.empty();
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getDocs/<?php echo $data->company->id;?>\');btnClick($(this))">Toate</div>');
        $.post("/companies/getServices/<?php echo $data->company->id?>", {}, function(data) {
            try {
                var resp = JSON.parse(data);
                
                if (resp.ok) {
                    $.each(resp.body.tableData, function(key, value) {
                        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getDocs/<?php echo $data->company->id?>/'+key+'\');btnClick($(this));">'+value.Categorie+'</div>');
                    });
                } else {
                    console.log(resp.info.message);
                }
            } catch(ex) {
                console.log(ex.message);
                console.log(data);
            }
        });
        
        lmenu.children(".btn:first-child").trigger("click");
    });

    $("[m-btn-requests]").click(function(e) {
        btnClick($(e.target));
        
        lmenu.empty();
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getRequests/<?php echo $data->company->id;?>\');btnClick($(this))">Toate</div>');
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getUnansweredRequests/<?php echo $data->company->id;?>\');btnClick($(this))">Fără răspuns</div>');
        lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getAnsweredRequests/<?php echo $data->company->id;?>\');btnClick($(this))">Cu răspuns</div>');
        lmenu.children(".btn:first-child").trigger("click");
    });

    function autoloadTable(url) {
        loadIntoTable(url, $("#table"), ["d", "e", "r"]);
    }

    $("[btn-min-max]").click(function(e) {
        var win = $("[win]");
        var btn = $("[btn-min-max]");

        if (win.hasClass("company-manager-box")) {
            win.removeClass("company-manager-box").addClass("bx-flex").addClass("bx-scroll-y-n").addClass("bx-scroll-h").addClass("mih-10");
            $("[table-area]").removeClass("h-5").addClass("bx-max-table");
            btn.removeClass("deg-0").addClass("deg-4");
            setCookie("memberview", "maximize", 1);
        } else {
            win.addClass("company-manager-box").removeClass("bx-flex").removeClass("bx-scroll-y-n").removeClass("bx-scroll-h").removeClass("mih-10");
            $("[table-area]").removeClass("bx-max-table").addClass("h-5");
            btn.removeClass("deg-4").addClass("deg-0");
            setCookie("memberview", "minimize", 1);
        }
    })
    </script>
</body>
</html>