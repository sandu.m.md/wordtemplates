<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Company name</title>
    <?php 
    Loader::load("HeadRequires");
    App::includeCss("css/comp.css");
    ?>

    <style>
    .box {
        background-color: transparent;
        box-shadow: none;
    }

    .wrapper {
        text-align: center;
    }

    a {
        text-decoration: none;
        color: inherit;
        display: flex;
        align-items: center;
    }

    .wrapper > .frame:first-child {
        flex: 0 1 45px;
    }

    </style>
</head>
<body>
    <div class="wrapper">
        <div class="frame bx-flex">
            
        </div>
        <div class="frame">
            <div class="viewarea">
                <!-- <div class="btn-prev-next prev-frame-btn">&#8249;</div> -->
                <!-- <div class="btn-prev-next next-frame-btn">&#8250;</div> -->
                <div class="lent">
                    <div class="lent-frame">
                        <div class="lent-frame-box">
                            <div class="box">
                                <div class="box-header">
                                    <span class="company-name">Choose needed services</span>
                                </div>
                                <div class="box-body" style="padding-top: 20px">
                                    <div class="frame">
                                        <div class="box-row">
                                            <div class="box-row-el">
                                                <?php foreach ($data->services as $key => $value) {?>
                                                    <div id="res-ch-serv-<?php echo$key?>" mins="<?php echo$value->duration?>" class="service service-checkbox-n" onclick="chooseService(this);"><span style="font-size: 150%;margin-right: 5px">&bull;</span> <span><?php echo$value->title?> (usualy takes <span><?php echo$value->duration?></span> minutes)</span></div>
                                                <?php }?>
                                            </div>
                                            <div class="box-row-el">
                                                <div class="calendar">
                                                    <div class="calendar-box">
                                                        <div class="calendar-box-month">
                                                            <div class="calendar-box-month-name"></div>
                                                        </div>
                                                        <div class="calendar-box-weekdays">
                                                            <span>Mo</span><span>Tu</span><span>We</span><span>Th</span><span>Fr</span><span>Sa</span><span>Su</span>
                                                        </div>
                                                        <div class="calendar-box-days"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-row-el">
                                                You can come at:
                                                <div class="day-sch"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="frame" id="finish-btn" style="margin-top: 30px">
                                        <div id="finish-info" class="info"></div>
                                        <a class="btn" href="#register-anchor">Finish</a>
                                    </div>
                                    <div class="frame" id="register-form" style="margin-top: 300px; background-color: #CCF381; padding: 25px 0; margin-bottom: 200px">
                                        <div id="register-info"></div>
                                        <form name="register" onsubmit="return registerSubmit();">
                                            <input type="hidden" name="reserve_date" value="0">
                                            <input type="hidden" name="datetime_start" value="0">
                                            <input type="hidden" name="duration" value="0">
                                            <input type="text" name="firstname" placeholder="Firstname">
                                            <input type="text" name="lastname" placeholder="Lastname">
                                            <input type="text" name="email_address" placeholder="Email address">
                                            <input type="text" name="phone_number" placeholder="Phone number">
                                            <button class="btn" style="margin-top: 20px">Submit</button>
                                        </form>
                                    </div>
                                    <div class="frame" id="register-anchor"></div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="lent-frame">
                        <div class="lent-frame-box">
                            <div class="box">
                                <div class="box-header">
                                    <div class="company-name"><?php echo$data->company->name?></div>
                                    <div class="company-info">
                                        <span class="info"><?php echo$data->company->address?></span>
                                        <span class="info"><?php echo$data->company->phone_number?></span>
                                    </div>
                                </div>
                                <div class="box-body centered">
                                    <div class="frame">
                                        <div class="row">
                                            <div class="row-el">
                                                <span class="title">We provide folowing services</span>
                                                <?php foreach ($data->services as $key => $value) {?>
                                                    <div class="service"><span style="font-size: 150%">&bull;</span><span><?php echo$value->title?></span></div>
                                                <?php }?>
                                            </div>
                                            <div class="row-el" style="padding-top: 40px">
                                                <div class="btn btn-lg" onclick="prev();">Reserve a meet</div>
                                                <div>or</div>
                                                <div class="btn btn-lg" onclick="next();">Contact online</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lent-frame">
                        <div class="lent-frame-box">
                            <div class="box">
                                <div class="box-header">
                                    <span class="company-name">Share your problem with us</span>
                                </div>
                                <div class="box-body">
                                    <div class="frame" style="background-color: #CCF381; padding-top: 30px; padding-bottom: 30px">
                                        <form name="contact" onsubmit="return contactSubmit();">
                                            <input type="text" name="firstname" placeholder="Firstname">
                                            <input type="text" name="lastname" placeholder="Lastname">
                                            <input type="text" name="email_address" placeholder="Email address">
                                            <input type="text" name="phone_number" placeholder="Phone number">
                                            <textarea name="description" id="" cols="30" rows="5" placeholder="Type your problem"></textarea>
                                            <button class="btn btn-lg">Send my problem</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#register-anchor" id="r-link"></a>
    <script>
    $(".lent").css("width", "700vw");
    
    var frames = [$(".lent-frame:nth-child(1)"), $(".lent-frame:nth-child(2)"), $(".lent-frame:nth-child(3)")];

    var currentFrame = 1;

    var frameWidth = parseInt($(".lent-frame").css("width"));
    
    var frameMarginError = Math.abs(frameWidth - parseInt($(".viewarea").css("width"))) / 2;    

    toCurrentFrame();

    setInterval(function(){
        frames = [$(".lent-frame:nth-child(1)"), $(".lent-frame:nth-child(2)"), $(".lent-frame:nth-child(3)")];

        frameWidth = parseInt($(".lent-frame").css("width"));
    
        frameMarginError = Math.abs(frameWidth - parseInt($(".viewarea").css("width"))) / 2;    

        toCurrentFrame();
    }, 1);


    

    function next() {
        if (currentFrame < 2) {
            currentFrame++;
            toCurrentFrame();
        }
    }

    function prev() {
        if (currentFrame > 0) {
            currentFrame--;
            toCurrentFrame();
        }
    }

    function toCurrentFrame() {
        if (currentFrame == 0) {
            $(".prev-frame-btn").hide();
            $(".next-frame-btn").show();
        } else if (currentFrame == frames.length - 1) {
            $(".prev-frame-btn").show();
            $(".next-frame-btn").hide();
        } else {
            $(".prev-frame-btn").show();
            $(".next-frame-btn").show();
        }
        $(".lent").css("left", (-1 * (frameWidth * currentFrame + frameMarginError)) + "px");
    }
    
    $(".next-frame-btn").click(function(e){
        next();
    });    
    
    $(".prev-frame-btn").click(function(e){
        prev();
    });    

    $(".box-row-el:nth-child(2)").hide();
    $(".box-row-el:nth-child(3)").hide();
    $("#finish-btn").hide();
    $("#register-form").hide();

    function chooseService(el) {
        var e = $(el);

        if (!e.hasClass("service-choosed")) {
            e.removeClass("service-checkbox-n");
            e.addClass("service-checkbox-y");
            e.addClass("service-choosed");

            if ($(".box-row-el:nth-child(2)").css("display") == "none") {
                $(".box-row-el:nth-child(2)").show();
                cal("", <?php echo$data->company->id?>); 
            }

            $("input[name=duration]").val(parseInt($("input[name=duration]").val()) + parseInt(e.attr("mins")));
        } else {
            e.removeClass("service-choosed");
            e.addClass("service-checkbox-n");
            e.removeClass("service-checkbox-y");
            $("input[name=duration]").val(parseInt($("input[name=duration]").val()) - parseInt(e.attr("mins")));
        }
    }

    function registerSubmit()
    {
        $.post("/companies/register/<?php echo$data->company->id?>", $("[name=register]").serializeArray(), function(data) {
            try {
                var resp = JSON.parse(data);

                if (resp.ok) {
                    alert("Successful register");
                    doAction({type: "redirect", url: "/"});
                } else {
                    console.log(resp.info.message);
                }
            } catch (ex) {
                console.log(ex.message);
                console.log(data);
            }
        });
        return false;
    }

    function contactSubmit()
    {
        $.post("/companies/contact/<?php echo$data->company->id?>", $("[name=contact]").serializeArray(), function(data) {
            try {
                var resp = JSON.parse(data);

                if (resp.ok) {
                    alert("Successful send");
                    doAction({type: "redirect", url: "/"});
                } else {
                    console.log(resp.info.message);
                }
            } catch (ex) {
                console.log(ex.message);
                console.log(data);
            }
        });
        return false;
    }

    </script>
</body>
</html>