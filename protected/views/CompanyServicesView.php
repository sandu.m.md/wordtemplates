<div class="bx-flex-row bx-full">
    <div class="bx-flex-el bx-flex-col fp-2">
        <div class="bx-flex-el bx-flex-col fp-0" left-menu>
            <div class="bx-flex-el bx-pad fp-0 bx-btn">Menu 1</div>
            <div class="bx-flex-el bx-pad fp-0 bx-btn">Menu 2</div>
            <div class="bx-flex-el bx-pad fp-0 bx-btn">Menu 3</div>
            <div class="bx-flex-el bx-pad fp-0 bx-btn">Menu 4</div>
        </div>
    </div>
    <div class="bx-flex-el bx-pad bx-brd-l">
        <div class="bx-flex-el bx-scroll-y-n h-8 bx-full-w" table-area>
            <div class="table">
                <table id="table">
                </table>
            </div>
        </div>
    </div>
</div>
<script>
var lmenu = $("[left-menu]");

lmenu.empty();
lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getDocs/<?php echo $data->company->id;?>\');btnClick($(this))">Toate</div>');
$.post("/companies/getServices/<?php echo $data->company->id?>", {}, function(data) {
    try {
        var resp = JSON.parse(data);
        
        if (resp.ok) {
            $.each(resp.body.tableData, function(key, value) {
                lmenu.append('<div class="bx-flex-el btn" onclick="autoloadTable(\'/companies/getDocs/<?php echo $data->company->id?>/'+key+'\');btnClick($(this));">'+value.Categorie+'</div>');
            });
        } else {
            console.log(resp.info.message);
        }
    } catch(ex) {
        console.log(ex.message);
        console.log(data);
    }
});

lmenu.children(".btn:first-child").trigger("click");

function autoloadTable(url) {
    loadIntoTable(url, $("#table"), ["d", "e", "r"]);
}

</script>