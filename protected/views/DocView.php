<div class="bx-fix bx-full-vw bx-flex-row-r bx-bg-dark-1 bx-txt-white bx-scroll-y-n" style="top: 0; left:0; hight: auto !important;" id="doc-box-window">
    <div class="bx-flex-el bx-flex-row-r fp-6" id="doc-box">
        <div class="bx-flex-el bx-flex-col bx-pad bx-pad-l-2 bx-pad-r-2 bx-bg-blue fp-0">
            <div class="bx-flex-el fp-0 bx-pad bx-txt-center bx-title-3"><?php echo $data->doc->name?></div>
            <div class="bx-flex-el bx-flex-row">
                <form id="doc-form" class="bx-flex-col bx-flex-el" onsubmit="return false;">
                    <div class="bx-flex-el grid "></div>
                    <div class="bx-flex-el"></div>
                    <div class="bx-flex-el fp-0">
                        <input type="submit" class="btn bx-bg-whitesmoke bx-txt-black bx-full-w" value="Descarcă" onclick="doAction({type:'download-file', url:'/templates/processDoc/<?php echo $data->doc->id;?>'}, 'doc-form');">
                    </div>
                </form>
            </div>
        </div>
        <div class="bx-flex-el we-5 bx-bg-white bx-full-w bx-pad-2 bx-txt-black" id="doc-view"></div>
    </div> 
</div>
<script>
getDocsFields();

loadInto("/templates/getView/<?php echo $data->doc->id;?>", $("#doc-view"));

$("#doc-box-window").click(function(e) {
//Hide the menus if visible
    $(e.target).hide();
});

$('#doc-box').click(function(e){
    e.stopPropagation();
});

function getDocsFields() {
    var dockey = <?php echo $data->doc->id?>;
    var form = $("#doc-form").children(".grid");

    $.post("/templates/getVariables", {templateId: dockey}, function(data){
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                if (resp.body !== false && resp.body !== true) {
                    form.empty();

                    $.each(resp.body, function(fname, fval) {
                        fieldsHtml = '<input class="doc_input" autocomplete="off" title="'+fname.replace(/\_/g, " ")+'" name="' + fname + '"';

                        for(const[attrName, attrValue] of Object.entries(fval)) {
                            fieldsHtml += ' ' + attrName + '="' + attrValue + '"';
                        }

                        fieldsHtml += '>';

                        form.append(fieldsHtml);
                    });
                } else {
                    console.log("Empty body");
                }
            } else {
                console.log("Bad resp");
            }
        } catch (ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}
</script>