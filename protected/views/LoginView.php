<?php
if (!isset($_COOKIE["username"]) || !isset($_COOKIE["password"])) {
    $usernameId = uniqid();
    $passwordId = uniqid();
    setcookie("username", $usernameId, time() + 30*300, "/");
    setcookie("password", $passwordId, time() + 30*300, "/");
} else {
    $usernameId = $_COOKIE["username"];
    $passwordId = $_COOKIE["password"];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Autentificare</title>
    <?php Loader::load("HeadRequires");?>
</head>
<body>
    <div class="wrapper mih-10 bx-flex bx-content-center bx-title-4">
        <div class="bx-flex-col bx-bg-whitesmoke we-3 bx-brd-l bx-brd-r bx-brd-b">
            <div class="bx-flex-el bx-flex bx-pad bx-bg-blue bx-title-1 bx-txt-white fp-0 bx-content-center">Autentificare</div>
            <form name="login" class="bx-flex-el bx-pad bx-flex-col" onsubmit="return authSubmit();">
                <div class="bx-flex-el bx-pad-05 fp-0">
                    <input type="text" name="<?php echo$usernameId?>" class="bx-brd bx-input bx-title-3 bx-full" placeholder="Numele de utilizător" autocomplete="new-password">
                </div>
                <div class="bx-flex-el bx-pad-05 fp-0">
                    <input type="password" name="<?php echo$passwordId?>" class="bx-brd bx-input bx-title-3 bx-full" placeholder="Parola" autocomplete="new-password">
                </div>
                <div class="bx-flex-el bx-flex bx-pad-05 bx-content-center">
                    <input type="checkbox" name="agree">Sunt de acord cu<pre> </pre><a class="bx-link-blue" href="#">Termenii de utilizare</a>
                </div>
                <div class="bx-flex-el bx-flex btn-flex-row bx-pad-05 fp-0">
                    <input type="submit" class="bx-btn bx-pad-05 bx-title-3 bx-flex-el bx-full" value="Conectare">
                </div>
                <div class="bx-flex-el bx-pad-l bx-pad-r bx-flex bx-content-center"><a class="bx-link-blue" href="/">Treci la pagina principală</a></div>
            </form>
        </div>
    </div>
    <script>
        function authSubmit() {
            if ($("input[name="+getCookie("username")+"]").val() == "" || $("input[name="+getCookie("password")+"]").val() == "") {
                notif("Introduceti datele necesare");
                return false;
            } 
            
            if ($("input[name=agree]:checked").length == 0) {
                notif("Nu ați confirmat că sunteți de acord cu Termenii de utilizare");
                return false;
            }

            $.post("/login/auth", $("form[name=login]").serializeArray(), function (data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        console.log("Success");
                        window.location.href = "/me/cab";
                    } else {
                        notif("Nume de utilizător sau parolă incorectă");
                        console.log(resp.info.message);
                    }
                } catch (ex) {
                    console.log(ex.message);
                    console.log(data);
                }
            });

            return false;
        }
    </script>
</body>
</html>