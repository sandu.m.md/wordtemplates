<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cabinet personal</title>
    <?php Loader::load("HeadRequires");?>
</head>
<body>
    <div class="wrapper bx-flex bx-flex-row mih-10">
        <div class="bx-flex-col  bx-bg-blue">
            <div class="bx-flex-el bx-pad bx-txt-center bx-bg-dark bx-txt-white bx-title-2 fp-1"><?php echo $data->user->firstname . " ".$data->user->lastname?></div>
            <div class="bx-flex-el bx-flex-col fp-15 bx-pad-t" main-menu>    
                <div class="bx-flex-el bx-pad bx-txt-white bx-btn fp-0" btn-profile>Profil</div>
                <div class="bx-flex-el bx-pad bx-txt-white bx-btn fp-0" btn-categories>Servicii</div>
                <div class="bx-flex-el bx-pad bx-txt-white bx-btn fp-0">Asistent</div>
                <div class="bx-flex-el bx-pad bx-txt-white bx-btn fp-0">Setări</div>
                <div class="bx-flex-el"></div>
                <?php foreach($data->companies as $company) {?>
                    <a href="/companies/memberview/<?php echo $company->id?>" class="bx-flex-el bx-pad bx-txt-white bx-btn fp-0"><?php echo $company->name?></a>
                <?php }?>
            </div>
        </div>
        
        <div class="bx-flex-el bx-flex-col bx-bg-whitesmoke">
            <div class="bx-flex-el bx-pad fp-0 bx-bg-blue bx-txt-white">
                <div class="left">Cabinet personal</div>
                <div class="right"><a class="bx-link-white" href="/auth/out">Ieși din cont</a></div>
            </div>
            <div class="bx-flex-el bx-flex-col bx-pad">
                <div class="bx-flex-el bx-bg-whitesmoke bx-flex" content-box>
                    Test
                </div>
            </div>
        </div>
    </div>
    <div id="pageadd"></div>
    <script>
    $("[btn-categories]").click(function(e) {
        bxBtnClick($(e.target));
        loadInto("/companies/getServicesView/<?php echo end($data->companies)->id?>", $("[content-box]"));
    });
    
    $("[btn-profile]").click(function(e) {
        bxBtnClick($(e.target));
        loadInto("/me/profile", $("[content-box]"));
    });

    $("[main-menu] > .bx-btn:first-child").trigger("click");

    </script>
</body>
</html>