<!DOCTYPE html>
<html>
<head>
	<title>Șabloane</title>
	<?php Loader::load("HeadRequires");?>
</head>
<body>
	<?php Loader::load("Navbar");?>
	<div class="wrapper">
		<div class="container centered w-8 top-margin top-padded">
			<?php if (App::$session->isActive()) { ?>
				<div class="container w-6 centered top-padded">
					<div class="search-box">
						<input type="text" class="search-field" id="search-box" placeholder="Search">
						<hr>
					</div>
					<div id="templates-list"></div>
				</div>
			<?php } else {Loader::load("Deny");} ?>
		</div>
	</div>
	<script>
		$("#templates-link").addClass("active");
		
		searchTemplates($('#templates-list'), 1, 10, {run: true, remove: false, edit: false, title: true}, "");
		
		$("#search-box").on('propertychange input', function(){
			searchTemplates($('#templates-list'), 1, 10, {run: true, remove: false, edit: false, title: true}, document.getElementById('search-box').value);
		});
	</script>
</body>
</html>